import 'regenerator-runtime/runtime';

export const fetchPlayerData= async (body) => {
  console.log('api.fetchChartData()...body ', body)
  try {
      let response = await fetch('/ajax?action=Sportlight:rehabAjax', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          //replicate jquery $.ajax() so server knows its ajax
          'X-Requested-With':'XMLHttpRequest',
        },
        body: JSON.stringify(body)
      })
    return await response.json()
  } catch(err) {
    console.log('error caught in api')
    console.log(err)
  }
}
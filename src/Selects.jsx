import React, { } from 'react';
import { } from 'react-router-dom';
//MUI
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';

const usePlayerSelectStyles = makeStyles(theme => ({
  root: {
    height:60,
    width:120,
    display:'flex',
    justifyContent:'flex-end',
    alignItems:'flex-start',
    //border:'solid'
  },
  select:{
    width:120,
  },
  player:{
    //display:'flex',
    //justifyContent:'flex-end'
  },
  playerText:{
    fontSize:11,
    color:'black'
  }
}));

export const PlayerSelect = ({players, selectedPlayer, onSelect}) =>{
  const classes = usePlayerSelectStyles();
  return(
    <div className={classes.root}>
      <Select className={classes.select}
        value={selectedPlayer ? selectedPlayer.name : ''}
        labelId="label" id="select" onChange={onSelect}>
        {players.map(player =>
          <MenuItem className={classes.player} value={player.name} key={player.name}>
              <span className={classes.playerText}>{player.name}</span>
          </MenuItem>
        )}
      </Select>
      {/**<PlayerDetails name={playerData.name} injury={playerData.injury}/>**/}
    </div>
  )
}
PlayerSelect.propTypes = {
}
PlayerSelect.defaultProps = {
  players:[],
  player:{}
}

const usePlayerDetailsStyles = makeStyles(theme => ({
  root: {

  },
  name:{
    textAlign:'right',
    fontSize:12,
    fontFamily: 'Arial, Helvetica, sans-serif',
    color:'black'
  },
  injury:{
    marginTop:3
  },
  injuryName:{
    textAlign:'right',
    fontSize:10,
    fontFamily: 'Arial, Helvetica, sans-serif',
    color:'#5a5de6',
  },
  injuryDate:{
    textAlign:'right',
    fontSize:10,
    fontFamily: 'Arial, Helvetica, sans-serif',
    color:'#5a5de6',
  }
}));
//todo - handle Date with moment.js
const PlayerDetails = ({name, injury}) =>{
  const classes = usePlayerDetailsStyles();
  return(
    <div className={classes.root}>
      <div className={classes.name}>{name || 'Name unknown'}</div>
      <div className={classes.injury}>
        <div className={classes.injuryName}>{injury.name || 'Injury unknown'}</div>
        <div className={classes.injuryDate}>{injury.date || 'Date unknown'}</div>
      </div>
    </div>
  )
}

PlayerDetails.propTypes = {
}
PlayerDetails.defaultProps = {
  injury:{}
}
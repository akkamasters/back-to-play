import React, { useState, useEffect } from 'react';
import { Link, Redirect, Route } from 'react-router-dom';
import { slide as ElasticMenu } from 'react-burger-menu';
import * as d3Regression from 'd3-regression';
import * as d3 from 'd3';
//MUI
import { makeStyles } from '@material-ui/core/styles';
//children
import { PlayerSelect } from './Selects';
import { MainMenu } from './Menus';
import Dashboard from './dashboard/Dashboard';
//data
import { mockRehabData } from './data/MockData';
import { rehabKPIs, kpis } from './data/KPIs';
import { getPlayerTargetsForKpi, getPortalData, addSublinks } from './data/DataProcessing';
import { fetchPlayerData } from './api';
import { getPlayerTargetDescriptors } from './data/TargetDescriptors';
import { addTargetsToPlayer, adjustedKpiScore, calcMedians } from './data/DataHelpers';
//daily chart functions
import rowTitles from './dashboard/charts/row-titles/RowTitles';
import dotPlots from './dashboard/charts/dot-plots/DotPlots';
import stagesTables from './dashboard/charts/stages-tables/StagesTables';
import sparklines from './dashboard/charts/sparkline/Sparklines';
//import lollipopChart from './dashboard/charts/lollipop-chart/LollipopChart';
//weekly chart functions
import weeklyTimeSeries from './dashboard/charts/weeklyTimeSeries/WeeklyTimeSeries';

const components = {
  weekly:[
    {id:'weeklyTimeSeries', name:'Weekly Tracker', chart:weeklyTimeSeries()}
  ],
  daily:[
    {id:'rowTitles', chart:rowTitles()},
    {id:'dotPlots', name:'% of target', chart:dotPlots()},
    {id:'stagesTables', name:'Stage completion', chart:stagesTables()},
    {id:'sparklines', name:'Progress', chart:sparklines()}
  ]
}

const useStyles = makeStyles(theme => ({
  root: {
    position:'relative'
  },
  playerDetails:{
    position:'absolute',
    top:'2vh',
    right:'2vw',
  },
  dashboard:{
    height:'calc(100vh - 56px)', //56px main portal header
    width:'calc(100vw - 90px)', //90px main portal side nav
    //for full screen
    //height:'100vh',
    //width:'100vw',
  },
  loading:{
    padding:60, color:'white', background:'#2F3234'
  },
  noData:{
    padding:60, color:'black', width:'600px'
  },
  hidden:{
    opacity:0
  }
}));

export default function Home(props) {
  const classes = useStyles();
  const portalData = getPortalData();
  const dataMode = (!portalData.team || portalData.team.customer_id === 'test') ? 'mock' : 'server';
  const [serverData, setServerData] = useState('');

  useEffect(() =>{
    if(dataMode === 'mock'){
      setServerData(mockRehabData)
    }else{
      fetchPlayerData({}).then((data) => {
          setServerData(data)
      })
    }
  },[])
  
  //selectedPlayer
  const { playerId } = props.match.params;
  //1st if is temp as mock data is diff structure
  if(dataMode === 'server'){
    if(serverData && serverData.players[0] && !playerId){
      //data has loaded - redirect to first player
      //temp - hardcode cork as default
      const firstPlayerId = 'A1557435989947947';//serverData.players[0].player_id;
      return(<Redirect to={'/rehab/'+firstPlayerId} />);
    }
  }
  else{
    //temp as mock data is diff structure
    if(serverData && !playerId){
      //data has loaded - redirect to first player
      const firstPlayerId = serverData[0].id;
      //console.log('redirecting to ', firstPlayerId)
      return(<Redirect to={'/rehab/'+firstPlayerId} />);
    }
  }

  var player, requiredKpis, playerSessions, playerDs;
  if(serverData && playerId){
    //temp if as mock data structure is diff - doesnt need processing
    if(dataMode === 'mock'){
      //playerData = serverData.find(p => p.id === playerId);
    }else{
      console.log('serverData', serverData)
      //NOTE - THE DATA IS UNFORMATTED AT THIS STAGE
      //all datapoints
      const allDs = serverData.sessions_with_kpis
        .map(sess => (
          sess.kpis.map(kpiObject =>({
              session_type_id:sess.session.session_type_id,
              ...kpiObject
            }))
        ))
        .reduce((acc,next) => [...acc, ...next],[])
        .map(d => ({...d, id:d.session_id +'-' +d.player_id}))

      //hardcoded for burnley session_type_id match = 7
      const matchDs = allDs.filter(d => d.session_type_id === '7');

      //player datapoints
      //filter sessions for selected player
      playerSessions = serverData.sessions_with_kpis
        .filter(sessWithKpis =>
            sessWithKpis.kpis[0] &&
            sessWithKpis.kpis.find(kpi => kpi.player_id === playerId))

      //KPIS
      requiredKpis = rehabKPIs(serverData.team.customer_id);
      //TARGETS
      const playerFromServer = serverData.players.find(p => p.player_id === playerId);
      const playerInjury = playerFromServer.injury || {
          //YYYYY-MM-DD
          date:  new Date('2020-05-03')
          //todo
          //rehabStartDate:last Sunday before injury 
          //(note: could be some ds before injury in that week)
      }
      const playerWithTargetDescriptors = {
        ...playerFromServer,
        injury:playerInjury,
        targetDescriptors:getPlayerTargetDescriptors(playerId, requiredKpis.all)
      }
      //add the actual targets based on descriptors
      player = addTargetsToPlayer(playerWithTargetDescriptors, matchDs);
    }
  }
  const links = portalData.links;
  //PLAYER SELECT HANDLER
  const handleSelectPlayer = e =>{
      const newPlayer = serverData.players.find(p => p.name === e.target.value)
      props.history.push('/rehab/'+newPlayer.player_id)
  }
  return(
    <>
      {playerSessions ?
        <section className={classes.root}>
          <div className={classes.playerDetails}>
            <PlayerSelect players={serverData.players} selectedPlayer={player}
              onSelect={handleSelectPlayer}/>
          </div>
          {playerSessions.length === 0 && <div className={classes.noData}>
                There is no session data for this player.
          </div>}
          <div className={classes.dashboard +' '+(playerSessions.length === 0 ? classes.hidden : '')}>
              <Dashboard player={player} playerSessions={playerSessions}
                kpis={requiredKpis} components={components}/>
          </div>
        </section>
        :
        <section className={classes.root}>
          <div className={classes.loading}>Loading...</div>
        </section>
      }
    </>
  );
}

Home.propTypes = {
}
Home.defaultProps = {
}


export const kpis = [
    {id:'distance_m', name:'Distance', shortName:'Distance', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'peak_speed_m_per_s', name:'Peak Speed', shortName:'Peak Sp', measure:'Speed', units:'m/s',
        defaults:{matchAvg:1}
    },

    {id:'speed_15_to_20_kph_distance_m', name:'Distance 15 - 20 km/h', shortName:'15-20kph', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'speed_20_to_25_kph_distance_m', name:'Distance 20 - 25 km/h', shortName:'20-25kph', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'speed_25_to_30_kph_distance_m', name:'Distance 25 - 30 km/h', shortName:'25-30kph', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'speed_30_plus_kph_distance_m', name:'Distance over 30 km/h', shortName:'>30kph', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'peak_acceleration_m_per_s2', name:'Peak Acceleration', shortName:'Peak Acc', measure:'Acceleration', units:'m/s/s',
        defaults:{matchAvg:1}
    },
    {id:'acceleration_3_to_6_m_per_s2_distance_m', name:'Accelerations 3 - 6 m/s/s', shortName:'Accel 3-6', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'acceleration_6_plus_m_per_s2_distance_m', name:'Accelerations 6+ m/s/s', shortName:'Accel >6', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'peak_deceleration_m_per_s2', name:'Peak Deceleration', shortName:'Peak Dec', measure:'Deceleration', units:'m/s/s',
        defaults:{matchAvg:1}
    },
    {id:'deceleration_3_to_6_m_per_s2_distance_m', name:'Decelerations 3 - 6 m/s/s', shortName:'Decel 3-6', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'deceleration_6_plus_m_per_s2_distance_m', name:'Decelerations 6+ m/s/s', shortName:'Decel >6', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'session_time_m', name:'Duration', shortName:'Time', measure:'Time', units:'min',
        defaults:{matchAvg:1}
    },
    //{id:'distance_m', name:'Distance', measure:'Distance', units:'m'},
    //{id:'peak_speed_m_per_s', name:'Peak Speed', measure:'Speed', units:'m/s'},
    {id:'average_speed_m_per_minute', name:'Metres Per Min', shortName:'M per min',  measure:'Avg Speed', units:'m/min',
        defaults:{matchAvg:1}
    },
    //decel / accel
    {id:'low_decelerations', name:'Low Decelerations', shortName:'Low Decel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'medium_decelerations', name:'Med Decelerations', shortName:'Med Decel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'high_decelerations', name:'High Decelerations', shortName:'High Decel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'low_accelerations', name:'Low Accelerations', shortName:'Low Accel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'medium_accelerations', name:'Med Accelerations', shortName:'Med Accel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    {id:'high_accelerations', name:'High Accelerations', shortName:'High Accel', measure:'Number', units:'Number of events',
        defaults:{matchAvg:1}
    },
    //distances
    {id:'high_speed_distance_m', name:'HSR Distance', shortName:'HSR Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'zone5_distance_m', name:'Zone 5 Distance', shortName:'Zone5 Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'sprint_distance_m', name:'Sprint Distance', shortName:'Sprint Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'high_power_distance_m', name:'High Power Distance', shortName:'High Power', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'explosive_distance_m', name:'Explosive Distance', shortName:'Explosive', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    //accel dist
    {id:'low_acceleration_distance_m', name:'Low Accel Dist', shortName:'Low Acc Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'medium_acceleration_distance_m', name:'Med Accel Dist', shortName:'Med Acc Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
    {id:'high_acceleration_distance_m', name:'High Accel Dist', shortName:'High Acc Dist', measure:'Distance', units:'m',
        defaults:{matchAvg:1}
    },
]
//helper
const onlyUnique = (value, index, self) => {
     return self.indexOf(value) === index;
}

export const rehabKPIs = customerId =>{
    var requiredKpis;
    switch(customerId){
        default:{
            requiredKpis = {
                weekly:[
                    'distance_m',
                    'high_speed_distance_m'
                ].map(id => kpis.find(kpi => kpi.id === id)),

                daily:[
                    'peak_speed_m_per_s',
                    'high_accelerations',
                    'high_speed_distance_m', 
                    'sprint_distance_m',
                ].map(id => kpis.find(kpi => kpi.id === id)),
            };
            break;
        }
    }
    return {
        ...requiredKpis,
        //add a prop that makes it easy to get all kpis eg for adding targets
        all:[...requiredKpis.weekly, ...requiredKpis.daily].filter(onlyUnique)
    }
}

export const kpiOptions = [
    {
        id:'distance_m',
        targets:[
            {priority:'high', value:7000, isMain:true},
            {priority:'medium', value:90, unit:'%'}
        ],
        progressTargets:[
            {priority:'medium', value:10, unit:'%'},
            {priority:'medium', value:-10, unit:'%'}
        ],
        stages:[
            {id:'s1', name:'S1', value:50, unit:'%'}, 
            {id:'s2', name:'S2', value:70, unit:'%'}, 
            {id:'s3', name:'S3', value:80, unit:'%'}, 
            {id:'s4', name:'S4', value:90, unit:'%'}
        ]
    },
    {
        id:'high_speed_distance_m',
        targets:[
            {priority:'high', value:610, isMain:true},
            {priority:'medium', value:90, unit:'%'}
        ],
        progressTargets:[
            {priority:'medium', value:10, unit:'%'},
            {priority:'medium', value:-10, unit:'%'}
        ],
        stages:[
            {id:'s1', name:'S1', value:50, unit:'%'}, 
            {id:'s2', name:'S2', value:70, unit:'%'}, 
            {id:'s3', name:'S3', value:80, unit:'%'}, 
            {id:'s4', name:'S4', value:90, unit:'%'}
        ]
    },
    {
        id:'sprint_distance_m',
        targets:[
            {priority:'high', value:260, isMain:true},
            {priority:'medium', value:90, unit:'%'}
        ],
        progressTargets:[
            {priority:'high', value:10, unit:'%', isMain:true},
            {priority:'medium', value:-10, unit:'%'}
        ],
        stages:[
            {id:'s1', name:'S1', value:50, unit:'%'}, 
            {id:'s2', name:'S2', value:60, unit:'%'}, 
            {id:'s3', name:'S3', value:75, unit:'%'}, 
            {id:'s4', name:'S4', value:90, unit:'%'}
        ]
    },
    {
        id:'peak_speed_m_per_s',
        targets:[
            {priority:'high', value:9.8, isMain:true},
            {priority:'medium', value:90, unit:'%'}
        ],
        progressTargets:[
            {priority:'high', value:10, unit:'%', isMain:true},
            {priority:'medium', value:-10, unit:'%'}
        ],
        stages:[
            {id:'s1', name:'S1', value:40, unit:'%'}, 
            {id:'s2', name:'S2', value:60, unit:'%'}, 
            {id:'s3', name:'S3', value:75, unit:'%'}, 
            {id:'s4', name:'S4', value:90, unit:'%'}
        ]
    },
    {
        id:'high_accelerations', 
        targets:[
            {priority:'high', value:35, isMain:true},
            {priority:'medium', value:90, unit:'%'}
        ],
        progressTargets:[
            {priority:'high', value:10, unit:'%', isMain:true},
            {priority:'medium', value:-10, unit:'%'}
        ],
        stages:[
            {id:'s1', name:'S1', value:30, unit:'%'}, 
            {id:'s2', name:'S2', value:50, unit:'%'}, 
            {id:'s3', name:'S3', value:70, unit:'%'}, 
            {id:'s4', name:'S4', value:90, unit:'%'}
        ]
    }

]







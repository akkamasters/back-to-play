import { mockLinks } from './MockData';
import { addDays, diffInWeeks } from './TimeHelpers';
import { kpis } from './KPIs';

/**
*	Removes data according to given filters, including removing players that are not required, 
*	and removing kpis and extraKpis from the remaining players
*
*	@param playersData array	the list of player data objects that has to be filterd
*	@param filters object       contains any filters that need to be applied, in the form of arrays of ids
*	
*	@return array               the filtered players with filtered kpis and extraKpis
**/
export const filterPlayersData = (playersData=[], filters={}) => {
	//console.log('playersData', playersData)
	const reqPlayerIds = filters.reqPlayerIds || [];
	const reqKpis = filters.reqKpis || [];
	const reqExtraKpis = filters.reqExtraKpis || [];
	return playersData
        .filter(pData => reqPlayerIds.includes(pData.id))
        .map(pData =>{
          //filter kpis
          const filteredKpis = pData.kpis ?
            pData.kpis.filter(kpi => reqKpis.includes(kpi.id))
            :[];
          const filteredExtraKpis = pData.kpis ? 
            pData.kpis.filter(kpi => reqExtraKpis.includes(kpi.id))
            :[];
          return{
            ...pData,
            kpis:filteredKpis,
            extraKpis:filteredExtraKpis
          }
        })
}

export const flattenDs = kpis => {
	if(!kpis)
		return [];
	return kpis.map(kpi => kpi.values).reduce((a,b) => [...a, ...b], []);
}
/**
*	@param value   string or number	the id of the player whose values should be returned

*	@return  
**/
const formatValue = (kpiId, value) => {
	//todo - may not need to check undefined - it just doesnt return which is same thing
	var numberValue;
	if(!value)
		return undefined;
	if(typeof value === 'number')
		numberValue = value;
	if(typeof value === 'string')
		numberValue = Number(value);

	//kpi specific formatting
	switch(kpiId){
		case 'peak_deceleration_m_per_s2':{
			return Math.abs(numberValue)
		}
		default:{
			return numberValue;
		}
	}
}
/**
*   gets an array of values for different sessions for a given player and kpi 
*
*	@param kpis array   the kpi defn objects, each with an id
*	@param data object  contains team object, players array, and sessions_with_kpis array,
*						which is an array of {session:{}, kpis:{}} objects - one per session
*
*   @return  array      - an array of playerData objects, 1 for each player in data.players
*                       - each playerData object contains {id, name, position, kpis}
*                       - each playerData.kpis contains [{...kpi, values}] where values is the 
*                         values for that player and kpi
**/
//kpiOptions passed in will overrride any that are already in the kpi

export const formatChartData = (kpis, players, sessions_with_kpis, kpiOptions=[]) =>{
	//opts can include targets, progressTargets, stages arrays
	const optionsForKpi = id =>{
		const options = kpiOptions.find(opts => opts.id === id)
		if(!options){
			return {};
		}
		return options;
	}

	return players.map(p =>{
		const playerKpis = kpis.map(kpi =>{
			const values = getPlayerValuesForKpi(p, kpi.id, sessions_with_kpis);
			return{
				...kpi,
				values:values,
				targets:p.targets.find(kpiTargets => kpiTargets.kpiId === kpi.id).targets,
				...optionsForKpi(kpi.id)
			}
		})
		return {
			id:p.player_id,
			position:p.position,
			name:p.name,
			kpis:playerKpis
		}
	})
}

/**
*   gets an array of values for different sessions for a given player and kpi 
*
*	@param players array   list of players
*
*   @return  array      - players in alph order, with id, firstName and secondName properties
						- surname is shortened to keep within 12 characters
**/
export const formatPlayers = players =>{
	return players
		.map(player =>{
			//split name into two
			const names = formatStr(player.name).split(' ');
			//2nd name max 12 chars
			const secondNameCut = names[1].slice(0,12)
			const secondName = names[1].length === secondNameCut.length ?
				names[1] : secondNameCut +'...'
			return{
				id:player.player_id || player.id || '',
				firstName:names[0] || '',
				secondName:secondName || '',
				pos:player.position || '',
				photoUrl:player.avatar || '' //???
			}
		})
		.sort((a,b) =>{
			if(a.secondName <= b.secondName)
				return -1;
			return 1;
		})
}

export const formatStr = str =>{
	if(typeof str !== 'string')
		return ''
	return str.replace('&lsquo;', '\'');
}

//note: data comes from server so each player has player_id not id at this stage
//helper
/*
*   gets an array of values for different sessions for a given player and kpi 
*
*	@param playerId         string	the id of the player whose values should be returned
*	@param kpiId            string  the id of the kpi whose value should be returned for each session
*	@param sessionsWithKpis array   1 object per session, each one containing a session object with details,
*									and a kpis object which has all the sessinPlayerKpi objects for that session 
*									ie 1 per player
*                           		ie [{session:{...}, kpis:[{sessionPlayerKpi},{sessionPlayerKpi},...]}]
*
* @return  array   the array of values and session details(from sessions which that player has a kpi value for)
* warning - values may be stored as undefined if some kpis exist and others dont
*/

export const getPlayerTargetsForKpi = (playerId, kpiId, preInjuryMatches) =>{
	//overrides
	//cork
	if(playerId === 'A1557435989947947'){
		if(kpiId === 'high_accelerations'){
			return [{ isMain:true, value:40 }];
		}
		if(kpiId === 'sprint_distance_m'){
			return [{ isMain:true, value:155 }];
		}
	}
	const preInjuryAdjustedScores = preInjuryMatches
		.map(sessWithKpi => 
			sessWithKpi.kpis.find(kpis => kpis.player_id === playerId))

		.map(sessKpi => {
			const score = Number(sessKpi[kpiId])
			//console.log('score', score)
			const time = Number(sessKpi['session_time_m'])
			//console.log('time', time)
			const adjustedScore = score * 90 / time
			//console.log('adjustedScore', adjustedScore)
			return adjustedScore;
		})
	//console.log('preInjuryAdjustedScores', preInjuryAdjustedScores)
	var preInjuryAvg;
	if(preInjuryAdjustedScores.length !== 0){
		preInjuryAvg = Number(d3.mean(preInjuryAdjustedScores).toFixed(1));
	}else{
		//todo
		//const kpi = passed thru instead of just kpiId
		//preInjuryAvg = kpi.defaults['matchAvg']
		preInjuryAvg = undefined;
	}
	//console.log('preInjuryAvg', preInjuryAvg)
	//todo - for eg peakspeed, use preInjurymax? in my view, it still makes sense to 
	//use avg peak speed as we are comparing the injured player to 
	//a typical preinjury performance, not their best ever performance
	//have it as a property of each kpi - which summary data to use
	//console.log('preInjuryAvg', preInjuryAvg)
	return [{
		isMain:true, 
		value:preInjuryAvg
	}];
}

//helper
//if 0 < diffInWeeks < 1, then it is 0, so we add 1 as its the 1st week (ie 1st bin since injury)
//same for later weeks.
//if diffInWeeks < 0, we will subtract 1 for the same reason 
const getWeekNr = (start, date) =>{
	const differenceInWeeks = diffInWeeks(start, date);
	if(start <= date){
		return differenceInWeeks + 1;
	}else{
		return differenceInWeeks - 1;
	}
}

export const getPlayerValuesForKpi = (player, kpiId, sessionsWithKpis) =>{
	//temp - save me changing to player_id
	const playerId = player.player_id;
	const values = sessionsWithKpis
		.map(sessWithKpis =>{
			//for this session, get the sessionPlayerKpis object for the required player
			const kpiObject = sessWithKpis.kpis.find(kpiObj => kpiObj.player_id === playerId);

			if(!kpiObject || !kpiObject[kpiId]){
				//either no kpi object for this player and session, or there is but no value for this kpi
				return undefined
			}else {
				var dateToUse;
				if(typeof sessWithKpis.session.date !== 'string'){
					dateToUse = sessWithKpis.session.date;
				}else{
					const yearMonthDay = sessWithKpis.session.date.split(' ')[0];
					//console.log('yearMonthDay', yearMonthDay)
					const basicDate = new Date(yearMonthDay)
					basicDate.setHours(0,0,0,0);
					dateToUse = basicDate;
				}
				return{
					date: dateToUse,// new Date(sessWithKpis.session.date),
					weekNr:getWeekNr(player.injury.date, dateToUse),
					//dont need sessionId as we have it in id
                    sessionId: kpiObject.session_id,
                    sessionTypeId: sessWithKpis.session.session_type_id,
                    //status: kpiObject.status,
                    title: sessWithKpis.session.title,
                    //uniqueId
                    id:kpiObject.session_id +'-' +playerId +'-'+kpiId,
                    value: formatValue(kpiId, kpiObject[kpiId])
				}
			}
		})
		.filter(valueWrapper=> valueWrapper !== undefined)
	
	const valuesWithMovingAvgs = values.map((v,i) =>({
		...v,
		movingAvg:calcMovingAvg(7, v.date, values)
	}))	
	return valuesWithMovingAvgs;
}

export const calcMovingAvg = (nrDays, date, values=[]) =>{
	const cutoff = addDays(0 - nrDays, date)
	const minDate = d3.min(values, v => v.date)
	//cant calc a moving avg if we dont have a full cycle of data
	//if(minDate > cutoff){
		//return undefined;
	//}

	//console.log('date', date)
	//console.log('cutoff', cutoff)
	//console.log('values', values)
	const valuesInRange = values.filter(v => 
		v.date >= addDays(0 - nrDays, date) && v.date <= date)
	//console.log('valuesInRange', valuesInRange)
	const tot = valuesInRange.map(v => v.value)
						     .reduce((total, next) => total+next, 0)
	//console.log('tot', tot)
	return tot / valuesInRange.length;
}

export const getPortalData = (players=[]) =>{
	var portalDataIsDeclared = true; 
	try{ rehabPortalData; }
	catch(e) {
	    if(e.name == "ReferenceError") {
	        portalDataIsDeclared = false;
	    }
	}

	if(portalDataIsDeclared === false){
		return {
			links:[
				  {name:'Sessions', url:'/sessions'},
				  {name:'Trends', url:'/trends'},
				  {name:'Players', url:'/players'},
				  {name:'Rehab', url:'/rehab', sublinks:[
				  	{name:'Peter Beardsley', url:'/rehab/peter-beardsley'},
				  	{name:'Ian Rush', url:'/rehab/ian-rush'},
				  	{name:'John Barnes', url:'/rehab/john-barnes'},
				  ]},
				  {name:'Workload', url:'/workload'},
				  {name:'Data', url:'/data'},
				  {name:'Log out', url:'/logout'},
				  {name:'Support', url:'/support'}
			]
		}
	}else{
		return rehabPortalData;
	}
}

export const addSublinks = (links, players) =>{
	const position = 3;
	const otherLinks = links.filter(l =>l.name !== 'Rehab')
	const rehabLink = {
		...links.find(l => l.name === 'Rehab'),
		sublinks:players.map(p => ({name:p.name, url:'/rehab/'+p.player_id}))
	}
	return [
		...otherLinks.slice(0,position), 
		rehabLink, 
		...otherLinks.slice(position, otherLinks.length)
	];
}






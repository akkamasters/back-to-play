const specificPlayerDescriptors = {
	//cork
	'A1557435989947947':{
		//default is westwoodMatchBest
		defaultDescriptors:[{
              isMain:true,
              type:'matchBest',
              //playerids can be all players from server.players
              playerIds:['A1557437077210210'], //westwood - change to all players
              percent:100,
              //no dateRange so all dates (daterange is an array [from,to])
        }],
		kpiDescriptors:{
			'high_accelerations':[{
				isMain:true,
                type:'manual',
                value:30
                //percent defaults to 100
			}],
			'peak_speed_m_per_s':[{
				isMain:true,
	            type:'matchBest',
	            sessionTimeCutoff:0,
	            //playerids can be all players from server.players
	            playerIds:['A1557435989947947'], //westwood - change to all players
	            percent:100,
	            //no dateRange so all dates (daterange is an array [from,to])
			}]
		}
	}
}

const generalPlayerDescriptors = playerId => ({
	defaultDescriptors:[{
		isMain:true,
	    type:'matchBest',
	    playerIds:[playerId],
	    percent:100,
	    //no dateRange so all dates (daterange is an array [from,to])

	}],
	kpiDescriptors:{
	}
})
export const getPlayerTargetDescriptors = (playerId, kpis) =>{
	const descriptors = specificPlayerDescriptors[playerId] || generalPlayerDescriptors(playerId);
	return kpis.map(kpi => ({
		kpiId:kpi.id,
		descriptors:descriptors.kpiDescriptors[kpi.id] || descriptors.defaultDescriptors
	}))
}



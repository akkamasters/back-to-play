//note - for baseValues,  0 is 0.1 to allow occasional changes ie 1
export const players = [
    {
        name:'Samuel Akingboye', player_id:'mockPlayer1', position:'Midfield',
        baseValues:[
            //Dante Bonfin
            {id:'distance_m', value: '8562'},
            {id:'peak_speed_m_per_s', value: '8.1'},
            {id:'speed_15_to_20_km_per_h', value:'1010.4'},
            {id:'speed_20_to_25_km_per_h', value:'196.9'},
            {id:'speed_25_to_30_km_per_h', value:'27.5'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.5'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'184.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'0.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-7.7'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'68.9'},
            {id:'deceleration_over_6_m_per_s_2', value:'2.6'}
        ],
        /*description:...inconsistent, some sessions excellent, others poor, but 
        also an underlying trend of improvement as the week went on
        //0.3, 0.8, 0.4, 0.7, 0.5
        //note: a good example of when the curveBasis is helpful
        */
        effortInSessions:[
            //week 1
            0.3, 0.9, 0.4, 0.8, 1,
            //week 2
            0.3, 0.9, 0.4, 0.8, 1,
            0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
            //0.3, 0.9, 0.4, 0.8, 1,
        ],
        /*
        the rate at which this player improves/regresses at each kpi per session 
        */
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1],
    },
    {
        name:'Aaron Haswell', player_id:'mockPlayer2', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...fairly solid effort all week, but had one very bad day
        */
        effortInSessions:[0.8, 0.9, 0.8, 0.8, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Steven Brokenshire', player_id:'mockPlayer3', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...one bad day in middle
        */
        effortInSessions:[0.9, 0.9, 0.3, 0.8, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Femi Oluwole', player_id:'mockPlayer4', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...very poor effort all week
        */
        effortInSessions:[0.4, 0.5, 0.3, 0.4, 0.6],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Viktor Nagy', player_id:'mockPlayer5', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...quite poor in training, worked hard in match
        */
        effortInSessions:[0.5, 0.7, 0.4, 0.5, 0.9],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'James Peterson', player_id:'mockPlayer6', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...const effort throughout
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Paulo Oviedo', player_id:'mockPlayer7', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Jermaine Richards', player_id:'mockPlayer8', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Denis McCall', player_id:'mockPlayer9', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Kwaku Olafi', player_id:'mockPlayer10', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Kevin Wheelan', player_id:'mockPlayer11', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.8, 0.8, 0.8, 0.8, 0.8],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Gyorgy Katas', player_id:'mockPlayer12', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Slavan Milenkovic', player_id:'mockPlayer13', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Juan Alberta', player_id:'mockPlayer14', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Jake Phillips', player_id:'mockPlayer15', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Jerome Anderson', player_id:'mockPlayer16', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Aaron Jones', player_id:'mockPlayer17', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Jamie Laidlaw', player_id:'mockPlayer18', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Callum McMahon', player_id:'mockPlayer19', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    },
    {
        name:'Francisco Trappini', player_id:'mockPlayer20', position:'Defence',
        baseValues:[
            //based on Patrick Bambu
            {id:'distance_m', value: '9018'},
            {id:'peak_speed_m_per_s', value: '7.5'},
            {id:'speed_15_to_20_km_per_h', value:'1114.2'},
            {id:'speed_20_to_25_km_per_h', value:'293.7'},
            {id:'speed_25_to_30_km_per_h', value:'15.3'},
            {id:'speed_over_30_km_per_h', value:'0.1'},
            {id:'peak_acceleration_m_per_s_2', value:'6.6'},
            {id:'acceleration_3_to_6_m_per_s_2', value:'210.2'},
            {id:'acceleration_over_6_m_per_s_2', value:'5.1'},
            {id:'peak_deceleration_m_per_s_2', value:'-8.0'},
            {id:'deceleration_3_to_6_m_per_s_2', value:'93.2'},
            {id:'deceleration_over_6_m_per_s_2', value:'3.7'}
        ],
        /*description:...
        */
        effortInSessions:[0.9, 1],
        /*
        */
        //one per kpi - dictates extend to which each kpi improves relative to base and to other kpis
        kpiImprovement:[1,1,1,1,1,1,1,1,1,1,1,1]
    }
]
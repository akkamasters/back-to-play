import { addWeeks } from './TimeHelpers';

const start = new Date(1603756800000);
const week2Date = addWeeks(1, start);
const week3Date = addWeeks(2, start);
const week4Date = addWeeks(3, start);
const week5Date = addWeeks(4, start);

const kpisPlayer1 = [
	{
		id:'HSR', name:'HSR', 
		targets:[
			{priority:'high', value:650, isMain:true},
			{priority:'medium', value:90, unit:'%'}
		],
		progressTargets:[
			{priority:'medium', value:10, unit:'%'},
			{priority:'medium', value:-10, unit:'%'}
		],
		values:[
		  {date:start, value:302},{date:week2Date, value:502}, {date:week3Date, value:512}, 
		  {date:week4Date, value:579},{date:week5Date, value:625}
		],
		stages:[
			{id:'s1', name:'S1', value:50, unit:'%'}, 
			{id:'s2', name:'S2', value:70, unit:'%'}, 
			{id:'s3', name:'S3', value:80, unit:'%'}, 
			{id:'s4', name:'S4', value:90, unit:'%'}
		]
	},
	{
		id:'sprintDistance', name:'Sprint',
		targets:[
			{priority:'high', value:149, isMain:true},
			{priority:'medium', value:90, unit:'%'}
		],
		progressTargets:[
			{priority:'high', value:10, unit:'%', isMain:true},
			{priority:'medium', value:-10, unit:'%'}
		],
		values:[
		  {date:start, value:92},{date:week2Date, value:101}, {date:week3Date, value:95}, 
		  {date:week4Date, value:120},{date:week5Date, value:127}
		],
		stages:[
			{id:'s1', name:'S1', value:50, unit:'%'}, 
			{id:'s2', name:'S2', value:60, unit:'%'}, 
			{id:'s3', name:'S3', value:75, unit:'%'}, 
			{id:'s4', name:'S4', value:90, unit:'%'}
		]
	},
	{
		id:'peakSpeed', name:'Peak Speed',
		targets:[
			{priority:'high', value:8.6, isMain:true},
			{priority:'medium', value:90, unit:'%'}
		],
		progressTargets:[
			{priority:'high', value:10, unit:'%', isMain:true},
			{priority:'medium', value:-10, unit:'%'}
		],
		values:[
		  {date:start, value:4.5},{date:week2Date, value:5.5}, {date:week3Date, value:5.2}, 
		  {date:week4Date, value:5.9},{date:week5Date, value:6.3}
		],
		stages:[
			{id:'s1', name:'S1', value:40, unit:'%'}, 
			{id:'s2', name:'S2', value:60, unit:'%'}, 
			{id:'s3', name:'S3', value:75, unit:'%'}, 
			{id:'s4', name:'S4', value:90, unit:'%'}
		]
	},
	{
		id:'highAccel', name:'High Accels',
		targets:[
			{priority:'high', value:10, isMain:true},
			{priority:'medium', value:90, unit:'%'}
		],
		progressTargets:[
			{priority:'high', value:10, unit:'%', isMain:true},
			{priority:'medium', value:-10, unit:'%'}
		],
		values:[
		  {date:start, value:1},{date:week2Date, value:3}, {date:week3Date, value:4}, 
		  {date:week4Date, value:5},{date:week5Date, value:6}
		],
		stages:[
			{id:'s1', name:'S1', value:30, unit:'%'}, 
			{id:'s2', name:'S2', value:50, unit:'%'}, 
			{id:'s3', name:'S3', value:70, unit:'%'}, 
			{id:'s4', name:'S4', value:90, unit:'%'}
		]
	},
	/*{
		id:'peakTurn60-120', name:'Peak Turn 60-120', numberOrder:'high-to-low',
		targets:[
			{priority:'high', value:0.7, isMain:true},
			{priority:'medium', value:90, unit:'%'}
		],
		values:[
		  {date:start, value:1.5}, {date:week2Date, value:1.45}, {date:week3Date, value:1.6}, 
		  {date:week4Date, value:1.32}, {date:week5Date, value:1.2}
		]
	}*/
]
export const mockRehabData = [
	{
		id:'peter-beardsley',
		name:'Peter Beardsley',
		injury:{
			name:'Meniscus rupture',
			date:'22 Oct, 1989'
		},
		kpis:kpisPlayer1
	},
	{
		id:'john-barnes',
		name:'John Barnes',
		injury:{
			name:'PCL rupture',
			date:'13 Nov, 1989'
		},
		kpis:kpisPlayer1
	},
	{
		id:'ian-rush',
		name:'Ian Rush',
		injury:{
			name:'Maisonneuve fracture',
			date:'21 Aug, 1989'
		},
		kpis:kpisPlayer1
	}
]
//this should be derived from data
export const mockLinks = [
	  {name:'Sessions', url:'/sessions'},
	  {name:'Trends', url:'/trends'},
	  {name:'Players', url:'/players'},
	  {name:'Rehab', url:'/rehab', sublinks:[
	  	{name:'Peter Beardsley', url:'/rehab/peter-beardsley'},
	  	{name:'Ian Rush', url:'/rehab/ian-rush'},
	  	{name:'John Barnes', url:'/rehab/john-barnes'},
	  ]},
	  {name:'Workload', url:'/workload'},
	  {name:'Data', url:'/data'},
	  {name:'Log out', url:'/logout'},
	  {name:'Support', url:'/support'}
]

//not needed now
const dashboardData = [
  {
    id:'volume', name:'Volume',
    metrics:[
      {
        id:'distance', name:'Distance (m)', 
        values:[
          {date:1, value:40},{date:2, value:55}, {date:3, value:65}, {date:4, value:70},{date:5, value:85}
        ]
      },
      {
        id:'HSR', name:'HSR (m)', 
        values:[
          {date:1, value:45},{date:2, value:45}, {date:3, value:55}, {date:4, value:45},{date:5, value:60}
        ]
      },
      {
        id:'sprintDistance', name:'Sprint (m)', 
        values:[
          {date:1, value:25},{date:2, value:30}, {date:3, value:35}, {date:4, value:30},{date:5, value:35}
        ]
      }
    ]
  },
  {
    id:'intensity', name:'Intensity',
    metrics:[
      {
        id:'peakSpeed', name:'Peak Speed', 
        values:[
          {date:1, value:20},{date:2, value:25}, {date:3, value:30}, {date:4, value:25},{date:5, value:30}
        ]
      },
      {
        id:'speed70Plus', name:'>70% of Max PS', 
        values:[
          {date:1, value:25},{date:2, value:35}, {date:3, value:35}, {date:4, value:45},{date:5, value:45}
        ]
      },
      {
        id:'speed90Plus', name:'>90% of Max PS', 
        values:[
          {date:1, value:10},{date:2, value:15}, {date:3, value:15}, {date:4, value:20},{date:5, value:25}
        ]
      }
    ]
  },
  {
    id:'turns', name:'Turns',
    metrics:[
      {
        id:'peakTurn', name:'Peak Turn', 
        values:[
          {date:1, value:10},{date:2, value:15}, {date:3, value:10}, {date:4, value:10},{date:5, value:15}
        ]
      },
      {
        id:'turns70Plus', name:'>70% of Max PT', 
        values:[
          {date:1, value:20},{date:2, value:25}, {date:3, value:25}, {date:4, value:35},{date:5, value:30}
        ]
      },
      {
        id:'turns90Plus', name:'>90% of Max PT', 
        values:[
          {date:1, value:10},{date:2, value:10}, {date:3, value:15}, {date:4, value:15},{date:5, value:15}
        ]
      }
    ]
  }
]


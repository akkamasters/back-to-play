import { diffInWeeks , addWeeks, addDays } from './util/TimeHelpers';

export const generateSessions = (startDate, nrWeeks=1) =>{
    const start = startDate || new Date();
    const weekStartDates = Array.from(Array(nrWeeks).keys())
        .map(weekNr => addDays(7*weekNr, start));

    console.log('weekStartDates', weekStartDates)
    const sessions = weekStartDates.map(start => generateSessionsForWeek(start))
                                   .reduce((a,b) =>[...a,...b],[])
    console.log('sessions', sessions)
    return sessions;
}
const generateSessionsForWeek = startDate =>{
    return([
        //week 1
        {
            //for 28 days ago, use addDays(-28, startDate) //mon - hard
            date:startDate, session_id:'mockSession1', session_type_id:'76', title:'Training',
            duration:90, difficulty:0.7
        },
        {
            //for 27 days ago  -27 //tue - hard
            date:addDays(1, startDate), session_id:'mockSession2', session_type_id:'76', title:'Training',
            duration:100, difficulty:0.8
        },
        {
            //for 26 days ago  use -26//mon - hard
            date:addDays(2, startDate), session_id:'mockSession3', session_type_id:'76', title:'Training',
            duration:90, difficulty:0.6
        },
        {
            //for 25 days ago use -25 //tue - hard
            date:addDays(3, startDate), session_id:'mockSession4', session_type_id:'76', title:'Training',
            duration:100, difficulty:0.2
        },
        {
            //for 24 days ago use -24 //mon - hard
            date:addDays(4, startDate), session_id:'mockSession5', session_type_id:'80', title:'Match',
            duration:90, difficulty:1
        }
    ])
}
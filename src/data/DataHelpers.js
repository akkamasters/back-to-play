import { addWeeks } from './TimeHelpers'

//player should have a targetDescriptors array of kpis
export const addTargetsToPlayer = (player, matchDs) =>	({
	...player,
    targets:player.targetDescriptors.map(kpiDescriptors =>({
    	kpiId:kpiDescriptors.kpiId,
      	targets:kpiDescriptors.descriptors.map(descriptor => 
        	createTarget(kpiDescriptors.kpiId, descriptor, matchDs))
    }))
})

//need to add 1 week on as we want the lower bound
const calcWeekStartDateFromWeekNr = (start, weekNr) => {
	if(weekNr === 0){
		return start;
	}else if(weekNr > 0){
		return addWeeks(weekNr - 1, start);
	}else{
		return addWeeks(weekNr, start);
	}
}
//no need to add one on as 
const calcWeekEndDateFromWeekNr = (start, weekNr) => {
	if(weekNr === 0){
		return start;
	}else if(weekNr > 0){
		return addWeeks(weekNr, start);
	}else{
		return addWeeks(weekNr + 1, start);
	}
}

const calcSummaryValue = (summary='total', valueKey='value', ds=[]) =>{
	const values = ds.filter(d => typeof d[valueKey] === 'number').map(d => d[valueKey]);
	switch(summary){
		case 'total':{
			return values.reduce((a,b) => a + b, 0)
		}
		case 'max':{
			return d3.max(values);
		}
		case 'mean':{
			if(values.length == 0){
				return undefined;
			}
			return values.reduce((a,b) => a + b, 0)/values.length
		}
		default: return undefined;
	}
}
//note - options.start should be a date object
//playerDs and data are not both needed, 
//but saves processing the ds again if we pass through from RehabHome

//player must have an injury.date property, assigned in RehabHome
export const addWeeklyData = (player, data=[], options={}) =>{
	//for now, assume if not data[0].values then same for rest
	if(!data[0] || data[0].values.length === 0){
		return data
	}
	const { requiredValueKeys, requiredSummaries, nrWeeksToShow } = options;
	const valueKeys = requiredValueKeys || ['value'];
	const summaries = requiredSummaries || ['total', 'max', 'min', 'median', 'mean'];
	const weeksToShowPre = nrWeeksToShow.pre || 26;

	//we get playerds here, with data that has been formatted
	const playerDs = data
		.map(kpi => kpi.values)
        .reduce((acc,next) => [...acc, ...next],[])

	const minDate = d3.min(playerDs, d => d.date);
	const maxDate = d3.max(playerDs, d => d.date);
	//note - start can be in the middle eg injury
	//console.log('minDate', minDate)
	const start = player.injury.date;

	const dataWithWeekly = data.map(kpi => {
		if(kpi.values.length === 0){
			return {
				...kpi,
				weeklyValues:[]
			}
		}

		const dsForKpi = playerDs.filter(d => {
			const splitId = d.id.split('-')
			const kpiId = splitId[splitId.length -1]
			return kpiId === kpi.id
		})

		const weeklyValuesNests = d3.nest()
			.key(d => d.weekNr)
			//.key(d => getWeekNr(start, d.date))
			.entries(dsForKpi)

		//console.log('weeklyValuesNests', weeklyValuesNests)
		const weeklyValues = weeklyValuesNests
			.map(nest =>{
				const weekNr = Number(nest.key);
				var summaryValues = {};
				valueKeys.forEach(key =>{
					const values = {};
					summaries.forEach(summary =>{
						values[summary] = calcSummaryValue(summary, key, nest.values);
					})
					summaryValues[key] = values;
				})
				return{
					kpiId:kpi.id,
					weekNr:weekNr,
					dateFrom:calcWeekStartDateFromWeekNr(start, weekNr),
					dateTo:calcWeekEndDateFromWeekNr(start, weekNr),
					values:nest.values,
					summaries:summaryValues
				}
			})
			//.filter(week => week.weekNr > -(weeksToShowPre))

		//console.log('weeklyValues', weeklyValues)
		const weeklyValuesWithChronic = weeklyValues.map(week =>{
			//console.log('week', week)
			const { weekNr } = week;
			const last4WeeksAll = [weekNr - 4, weekNr - 3, weekNr - 2, weekNr - 1]
				.map(wkNr => weeklyValues.find(wk => wk.weekNr === wkNr))

			//console.log('last4WeeksAll', last4WeeksAll)
			const last4Weeks = last4WeeksAll.filter(wk => wk !== undefined);
			//console.log('last4Weeks', last4Weeks)

			//undefined are filtered out for case of 1st 3 weeks or where there are gaps
			//todo - remove bands if less than 4 weeks
			//...for now keep them in and just get avg of what is there
			const chronic = {};
			Object.keys(week.summaries).forEach(valueKey =>{
				chronic[valueKey] ={};
				Object.keys(week.summaries[valueKey]).forEach(summary =>{
					//may be less than 4 weeks if gaps in data and at start
					const last4WeeksValues = last4Weeks.map(wk => {
						//console.log('last4=...', wk)
						return wk.summaries[valueKey][summary]
					}).filter(value => typeof value === 'number');
					const last4WeeksMean = last4WeeksValues.length === 0 ? undefined :
						last4WeeksValues.reduce((a,b) => a + b, 0) / last4WeeksValues.length;
					//todo - handle numberOrder == 'high to low' and negative numbers
					//check has peakDecel been made positive? I think so
					const lower = typeof last4WeeksMean === 'number' ? last4WeeksMean * 0.8 : undefined;
					const upper = typeof last4WeeksMean === 'number' ? last4WeeksMean * 1.2 : undefined;
					const upper2 = typeof last4WeeksMean === 'number' ? last4WeeksMean * 1.5 : undefined;

					//console.log('last4WeeksMean', last4WeeksMean)
					//same nr for now
					chronic[valueKey][summary] = {
						lower: lower, 
						upper:upper,
						upper2:upper2,
						mean: last4WeeksMean
					};
				})

			})
			return{
				...week,
				chronic:chronic
			}
		})

		//console.log('weeklyValuesWithChronic', weeklyValuesWithChronic)

		//todo - calc summary data
		return {
			...kpi,
			weeklyValues:weeklyValuesWithChronic
		}
	})

	//bounds - x axis is weeknr not date
	const allWeeks = dataWithWeekly.map(kpi => kpi.weeklyValues)
								   .reduce((a,b) => [...a, ...b], [])
	if(allWeeks.length === 0){
		return dataWithWeekly;
	}
	//console.log('allWeeks', allWeeks)
	function range(start, end) {
	  return Array(end - start + 1).fill().map((_, idx) => start + idx)
	}
	var fullDomain;
	if(allWeeks.length !== 0){
		const xMin = d3.min(allWeeks, v => v.weekNr);
		const xMax = d3.max(allWeeks, v => v.weekNr);
		fullDomain = range(xMin, xMax);
	}else{
		fullDomain = [addWeeks(-12, player.injury.date), addWeeks(12, player.injury.date)]
	}
	//console.log('fullDomain', fullDomain);


	//if gap > 12, remove those 12 and replace with a pseudo week
	//then give each a displayweeknr value
	//helpers
	const gapsToNearestNumber = (numberToTest, numbers) =>{
		const differences = numbers.map(num => num - numberToTest);

		const gapToNearestSmaller = Math.abs(d3.max(differences.filter(diff => diff < 0)));
		const gapToNearestLarger = d3.min(differences.filter(diff => diff > 0));
		return {
			down:gapToNearestSmaller,
			up:gapToNearestLarger
		}
	}

	const distanceDownToNextNumber = (numberToTest, numbers) =>{

		//todo - if numbers contains n return 0
		const lowerNumbers = numbers.filter(n => n < numberToTest)
		return numberToTest - d3.max(lowerNumbers);
	}
	const distanceUpToNextNumber = (numberToTest, numbers) =>{
		//todo - if numbers contains n return 0
		const higherNumbers = numbers.filter(n => n > numberToTest)
		return d3.min(higherNumbers) - numberToTest;
	}

	const allWeekNrs = allWeeks
		.map(wk => wk.weekNr)
		.filter(onlyUnique)
		.sort((a,b) => b.weekNr - a.weekNr)
	//console.log('allWeekNrs with data', allWeekNrs)

	const hasData = wkNr => allWeekNrs.find(weekNr => weekNr === wkNr);
	const domainWithPseudos = fullDomain.map((wkNr,i) => {
		if(hasData(wkNr)){
			//console.log('has Data')
			return {
				weekNr:wkNr,
				isPseudo:false,
				hasData:true
			}
		}else{
			const smallestAcceptableGap = 6;
			const prevWeekHasData = distanceDownToNextNumber(wkNr, allWeekNrs) === 1;
			const distanceUpToNextWeekWithData = distanceUpToNextNumber(wkNr, allWeekNrs);
			//if prev week has data, and the next week with data is > 8 weeks away
			if(prevWeekHasData && distanceUpToNextWeekWithData > smallestAcceptableGap){
				//console.log('replacing with pseudo')
				return{
					weekNr:wkNr,
					isPseudo:true
				}
			}
			if(prevWeekHasData){
				//console.log('not replacing with pseudo as next data is less than 6 weeks away')
			}else{
				//console.log('no need to replace - will be filtered as it is after a pseudo')
			}
			//no data but there is data within an acceptable nr of weeks
			//so replace with pseudo 
			//note: the following ones will stay and be filtered out in next step until one with data
			return {
				weekNr:wkNr,
				isPseudo:false
			}
		}
	})
	//helpers
	const closerToPseudoThanDataDown = (weekNr, weeks) =>{
		const min = d3.min(weeks, w => w.weekNr)
		const checkNext = i =>{
			if(i < min){
				return 'neither';
			}
			const week = weeks.find(w => w.weekNr === i);
			if(!week){
				//this shouldnt ever happen - if so, move on
				return checkNext(i-1);
			}
			if(week.isPseudo){
				return true;
			}
			if(hasData(week.weekNr)){
				return false;
			}
			return checkNext(i-1);
		}
		return checkNext(weekNr-1);
	}

	//console.log('domainWithPseudos', domainWithPseudos)
	const filteredDomainWithPseudos = domainWithPseudos.filter((wk,i) =>{
		if(hasData(wk.weekNr) || wk.isPseudo || wk.weekNr === 0){
			return true;
		}
		if(i == 0){
			return true;
		}
		if(closerToPseudoThanDataDown(wk.weekNr, domainWithPseudos)){
			return false;
		}
		return true;
	})

	//console.log('filteredDomainWithPseudos', filteredDomainWithPseudos)

	const domain = filteredDomainWithPseudos.map((wk,i) => ({
		weekNr:wk.weekNr,
		isPseudo:wk.isPseudo,
		displayWeekNr:i+1,
	}));
	//console.log('domain', domain)

	dataWithWeekly.weekNumbers = domain;
	dataWithWeekly.summaries = summaries;
	dataWithWeekly.valueKeys = valueKeys;

	return dataWithWeekly;
}

export const adjustedKpiScore = (d,kpi, adjustTo=96) =>{
        const time = Number(d['session_time_m']);
        const value = Number(d[kpi.id]);
        if(time === 0){
          return 0;
        }else{
          return (value / time) * adjustTo;
        }
      }
//todo - reverse the sort if numberorder = 'high-to-low'
//todo - handle datarange param
//cutoff defaults to 75 minutes
export const calcMatchBest = (kpiId, playerIds=[], matchDs, options={}) =>{
	const { dateRange, sessionTimeCutoff } = options;
	const _sessionTimeCutoff = typeof sessionTimeCutoff === 'number' ? sessionTimeCutoff : 75;
	const ds = matchDs.filter(d => d[kpiId])
	              .filter(d => Number(d['session_time_m']) >= _sessionTimeCutoff)
	              .filter(d => playerIds.includes(d.player_id));
	//console.log('ds', ds)
	const values = ds.map(d => Number(d[kpiId]));
	//console.log('values', values)
	return d3.max(values);
}

export const calcMedians = (kpis, ds, options={}) =>{
	const { adjust } = options;
	return kpis.map(kpi => {
	  const ordered = ds.filter(d => d[kpi.id])
	                    .map(d => {
	                      if(adjust){
	                        return adjustedKpiScore(d, kpi, adjust);
	                      }else{
	                        return Number(d[kpi.id]);
	                      }
	                    })
	                    .sort((d1,d2) => d1 - d2);

	  if(ordered.length === 0){
	    return {id:kpi.id, value:undefined};
	  }
	  if(ordered.length % 2 !== 0){
	    //odd
	    const middleNr = (ordered.length + 1)/2
	    const median = ordered[middleNr - 1]
	    return { id:kpi.id, value:Number(median.toFixed(1)) }
	  }else{
	    //even
	    const middleNrs = [ordered.length/2, (ordered.length/2)+1]
	    const firstMiddle = ordered[middleNrs[0]-1]
	    const secondMiddle = ordered[middleNrs[1]-1]
	    const median = (firstMiddle + secondMiddle)/2;
	    return { id:kpi.id, value:Number(median.toFixed(1)) };
	  }
	})
}

export const onlyUnique = (value, index, self) => {
     return self.indexOf(value) === index;
}

const createTarget = (kpiId, descriptor, matchDs) =>{
	//console.log('createTarget', kpiId)
	//console.log('matchDs', matchDs)
	//console.log('descriptor', descriptor)
    const { playerIds, dateRange, percent, value, sessionTimeCutoff } = descriptor
    //default
    const _percent = percent || 100;
    var _value, usedPlayerIds;
    switch(descriptor.type){
      case 'matchBest':{
        const options = { dateRange, sessionTimeCutoff };
        _value = calcMatchBest(kpiId, playerIds, matchDs, options) * _percent/100;
        //console.log('matchbest', _value)
        if(!_value){
        	//console.log('matchbest !val so using all players')
        	//calcMatchBest has returned undefined due to no matchds for playerIds
        	//default to using all players at 90pc
        	usedPlayerIds = matchDs.map(d => d.player_id).filter(onlyUnique)
        	//todo - replace percent with 90
        	_value = calcMatchBest(kpiId, usedPlayerIds, matchDs, options) * _percent/100;
        }
        break;
      }
      default:{
        //if manual, then there should be a value property
        _value = value * _percent /100;
        break;
      }
    }

    return{
      isMain:descriptor.isMain,
      requiredPlayerIds:playerIds,
      usedPlayerIds:usedPlayerIds,
      value:_value
    }
}


//data is an array of kpis
export const dataAsPercentages = data =>{
	return data.map(kpi =>({
		...kpi,
		targets:!kpi.targets ? undefined: 
			kpi.targets.map(t =>({
				...t,
				rawValue:t.unit !== '%' ? t.value : undefined,
				value:t.unit === '%' ? t.value : 100
			}))
		,
		rawUnits:kpi.units,
		units:'%',
		values:kpi.values.map(v =>({
			...v,
			rawValue:v.value,
			//todo - remove value as pcValue more descriptive
			value:valueAsPercentage(v.value, kpi),
			pcValue:valueAsPercentage(v.value, kpi),
			rawMovingAvg:v.movingAvg,
			movingAvg:valueAsPercentage(v.movingAvg, kpi)
		}))
	}))
}

//data is an array of kpis
export const weeklyDataAsPercentages = data =>{
	return data.map(kpi =>({
		...kpi,
		weeklyValues:kpi.values.map(v =>({
			...v,
			rawValue:v.value,
			value:valueAsPercentage(v.value, kpi),
			//note: prev weeks movingAvg is used for C in C:A ratio
			//and this weeks movingAvg is used for planning next weeks load
			rawMovingAvg:v.movingAvg,
			movingAvg:valueAsPercentage(v.movingAvg, kpi)
		}))
	}))
}

export const dataAsPercentageChange = data =>{
	return data.map(kpi =>({
		...kpi,
		/*targets:kpi.targets.map(t =>({
			...t,
			rawValue:t.unit !== '%' ? t.value : undefined,
			value:t.unit === '%' ? t.value : 100
		})),*/
		rawUnits:kpi.units,
		units:'%',
		values:kpi.values.map((v,i) =>{
			const previousValue = i === 0 ? null : kpi.values[i-1].value;
			return {
				...v,
				rawValue:v.value,
				value:valueAsPercentageChange(v.value, previousValue)
			}
		}),
		targets:kpi.progressTargets
	}))
}




export const valueAsPercentage = (value, kpi) =>{
	const target = kpi.targets.find(t => t.isMain) || kpi.targets[0];
	const targetValue = target.value;
	if(kpi.numberOrder === 'high-to-low'){
		if(value === 0){
			if(targetValue === 0)
				return 100;
			else
				return 0;
		}
		return (targetValue/value)*100;
	}else{
		//if target is 0, then player has already met target (and we dont want to 
		//show it as being above 100% as it could go off the scale)
		if(targetValue === 0)
			return 100;
		return (value/targetValue)*100;
	}
}

//NOTE - FOR NOW, MAX CHANGE IS 20PC
//returns 100%if previous value is 0
//todo - be smarter - give it a default based on other increases
// so it is a smooth step
export const valueAsPercentageChange = (value, previousValue, defaultChange=10) =>{
	if(typeof previousValue !== 'number'){
		return 0;
	}
	if(value === previousValue){
		return 0;
	}
	if(value > previousValue){
		//handle case of denom = 0
		if(previousValue === 0){
			//console.log('returning default 10')
			return defaultChange;
		}
		const actualChange = value - previousValue;
		const pcChange = (actualChange/previousValue) *100;
		return Math.min(pcChange, 20);
	}else{
		if(previousValue === 0){
			//console.log('returning default - 10')
			return 0 - defaultChange;
		}
		const actualChange = previousValue - value;
		const pcChange = 0 -(actualChange/previousValue) *100;
		return Math.max(pcChange, -20);
	}

	
}





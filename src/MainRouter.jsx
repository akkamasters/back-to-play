import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Route, Switch }from 'react-router-dom'
//styles 
import "./App.css";
//children
import { PageTemplate } from './PageTemplate'
import RehabHome from './RehabHome';

class MainRouter extends Component {
  render() {

    return(
      <PageTemplate>
        {<Switch>
          <Route path="/rehab/:playerId" component={RehabHome}/>
           <Route component={RehabHome}/>
        </Switch>}
      </PageTemplate>)
  } 
}

export default MainRouter

import React from 'react'
import {NavLink, Link, withRouter} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root:{

  },
  list:{
    //maxHeight:'80vh',
    width:150,
    marginTop:'5vh',
    overflowY:'auto',
    padding:0,
    //border:'solid'
  },
  item:{
    margin:'2vh',
    marginLeft:'30px', //good for portal, but not for npm start
    cursor:'pointer',
    listStyleType:'none',
    //border:'solid',borderColor:'yellow'
  },
  sublinks:{
    maxHeight:'230px',
    marginTop:'2vh',
    marginLeft:'20px',
    //border:'solid',borderColor:'blue',
    padding:0,
    overflowY:'auto'
  },
  sublinkItem:{
    height:'40px',
    width:70,
    margin:'0vh 0px 2vh',
    cursor:'pointer',
    listStyleType:'none',
    //border:'solid',
    lineHeight: 1
  },
  activeLink:{
    color:'white',
    fontSize: 13,
    "&:hover": {
      color:'white'
    }
  },
  inactiveLink:{
    color: '#383E42',
    fontSize: 13,
    "&:hover": {
       color: '#383E42'
    }
  }
}));

export const MainMenu = withRouter(({links, history, logo}) =>{
  const classes = useStyles();
  //helper
  const isActive = url => {
    //return (history.location.pathname.startsWith(path))
    return history.location.pathname.includes(url);
  }

  const splitName = name => {
    const split = name.split(' ');
    if(split.length == 0){
      return ['', ''];
    }
    if(split.length == 1){
      return [split.length[0], ''];
    }
    if(split.length == 2)
      return split;
    return [split[0], split.slice(1).join(' ')];
  }
  return(
    <div className={classes.root}>
        {logo && <div className={classes.logoCont} onClick={() =>history.location.push('/')}>
            <img src={logo} />
        </div>}
        <ul className={classes.list}>
          {links.map(link =>
              <li className={classes.item}
                key={'link-'+link.url}>
                 <a href={link.url} className={isActive(link.url) ? classes.activeLink : classes.inactiveLink}>
                    {link.name}
                 </a>
                 {link.sublinks && 
                  <ul className={classes.sublinks}>
                    {link.sublinks.map(sublink =>
                      <li className={classes.sublinkItem} key={'sublink-'+sublink.name}>
                        <NavLink to={sublink.url}
                          className={isActive(sublink.url) ? classes.activeLink : classes.inactiveLink}>
                          <div>{splitName(sublink.name)[0]}</div>
                          <div>{splitName(sublink.name)[1]}</div>
                        </NavLink>
                      </li>)}
                 </ul>}
              </li>
          )}
        </ul>
    </div>
  )}
)
MainMenu.defaultProps = {
  links:[]
}

const usePlayerMenuStyles = makeStyles(theme => ({
  rootNormal: {
  },
  rootElastic:{

  },
  list:{
    maxHeight:'80vh',
    marginTop:'5vh',
    overflowY:'auto'
  },
  listElastic:{

  },
  item:{
    margin:'2vh',
    marginLeft:'30px',
    height:'30px',
    cursor:'pointer',
  },
  activeItem:{
    color:'white'

  }
}));


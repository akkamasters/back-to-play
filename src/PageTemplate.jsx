import React, { Component, useState } from 'react'
import { render } from "react-dom"
import { Link, withRouter} from 'react-router-dom'
import { slide as ElasticMenu } from 'react-burger-menu'
import { makeStyles } from '@material-ui/core/styles';
//children
import { MainMenu } from './Menus';
//helpers
import { getPortalData } from './data/DataProcessing';

const useStyles = makeStyles(theme => ({
  root: {
  	background: '#f9f9f9',
  },
  content:{
  }
}));

export const PageTemplate = withRouter(({children, history, match}) =>{
	const classes = useStyles();

	//get any relevant data from the referrer ie the portal main app
	//const portalData = getPortalData();

	return(
		<section className={classes.root}>
			{/**<div className={'page-background-cont '}></div>**/}
			{/**<div className='bm-burger-button'></div>**/}
	        {/**<ElasticMenu left width={200}>
	          <MainMenu links={links} elastic />
	        </ElasticMenu>**/}
	        <div className={classes.content}>
				{children}
			</div>
		</section>
	)}
)



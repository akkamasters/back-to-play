import React, {useState, useEffect, useLayoutEffect}  from 'react';
import * as d3 from 'd3';
//import PropTypes from 'prop-types'
//import debounce from 'lodash/debounce'
//material-ui
import { makeStyles } from '@material-ui/core/styles';
//children
import DataComponent from './data-components/DataComponent';
import WeeklyTrends from './WeeklyTrends';
import ChronicAcutePlanner from './ChronicAcutePlanner';
//helpers
import { addWeeklyData, dataAsPercentages, dataAsPercentageChange, 
    valueAsPercentage } from '../data/DataHelpers';
import { formatChartData } from '../data/DataProcessing';
import { addWeeks } from '../data/TimeHelpers';

const useStyles = makeStyles(theme => ({
	root:{
    height:'100%',
  },
  topPanel:{
    height:'calc(42vh - 56px)',
    padding:'2vh 2vw 1vh 2vw',
    display:'flex',
  },
  mainArea:{
    height:'58vh',
    padding:'2vh 2vw 1vh 2vw',
    position:'relative',
  	display:'flex',
  },
  //WEEKLY
  weeklyTrends:{
    flex:'75% 1 1',
    height:'100%',
    color:'black',
  },
  weeklyTable:{
    flex:'280px 0 0',
    minWidth:'220px',
    alignSelf:'flex-end',
    padding:'45px 0 25px 0',
  },
  //MAIN AREA
  //1.DAILY
  dailyComponents:{
    flex:'85% 1 1',
  	height:'100%',
  	display:'flex',
    paddingRight:'50px'
  },
  //2.TURNS
  turns:{
    flex:'280px 0 0',
    minWidth:'220px',
    height:'100%',
    padding:'40px 0 30px 0'
  },
  gantts:{
    height:'100%',
    width:'100%',
    padding:'1vh 1vw',
    background:'#E8E8E8',
    color:'black',
    fontSize:12,
    fontWeight:'bold'

  },
  dailyTitle:{
    position:'absolute',
    left:'2vw',
    top:22,
    color:'black',
    fontSize:'12px',
    fontWeight:'bold',
  },
  rowTitles:{
    height:'100%',
    flex:'80px 0 0'
  },
	dotPlots:{
    height:'100%',
    flex:'50% 1 1',
	},
	stagesTables:{
    height:'100%',
		flex:'20% 0 1',
    minWidth:'150px',
	},
	sparklines:{
    height:'100%',
    flex:'30% 1 1',
    maxWidth:'300px',
	},
  noData:{
    padding:'50px',
    color:'black'
  }
}));

const Dashboard = ({player, playerSessions, kpis, components}) =>{
  //state
  const [hoveredWeekNr, setHoveredWeekNr] = useState('');
  const [weeklySummary, setWeeklySummary] = useState('total');
  const [charts, setCharts] = useState([]); 
  const injury = player.injury || {date:new Date()};
  const classes = useStyles();
  //DATA
  //1.DAILY DATA
  const playerData = formatChartData(kpis.all, [player], playerSessions)[0];
  const percentageData = dataAsPercentages(playerData.kpis);
  const filtered = percentageData .filter(kpi => kpis.daily.find(dailyKpi => dailyKpi.id === kpi.id))
      .map(kpi =>({
        ...kpi, 
        values:kpi.values.filter(d => d.date >= injury.date)
      }))

  const dailyData = percentageData
      .filter(kpi => kpis.daily.find(dailyKpi => dailyKpi.id === kpi.id))
      .map(kpi =>({
        ...kpi, 
        values:kpi.values.filter(d => d.date >= injury.date)
      }))

  const orderedDailyData = dailyData.length <= 1 ? dailyData :
   dailyData.sort((kpi1,kpi2) => {
      if(kpi1.values == 0 && kpi2.values != 0){
        return -1;
      }else if(kpi1.values != 0 && kpi2.values == 0){
        return 1;
      }else if(kpi1.values == 0 && kpi2.values == 0){
        return 1;
      }else{
        const bestValueKpi1 = d3.max(kpi1.values, v => v.value);
        const bestValueAsPercentKpi1 = valueAsPercentage(bestValueKpi1, kpi1);
        const bestValueKpi2 = d3.max(kpi2.values, v => v.value);
        const bestValueAsPercentKpi2 = valueAsPercentage(bestValueKpi2, kpi2);
        return bestValueAsPercentKpi2 - bestValueAsPercentKpi1;
      }
  })

  //2. WEEKLY DATA
  //todo - pass the following values in from database user
  const nrWeeksToShow = {
    pre:35,
    post:undefined
  }
  const weeklyOptions ={
    requiredValueKeys:['rawValue', 'pcValue'], 
    requiredSummaries:['total', 'max', 'mean'],
    nrWeeksToShow:nrWeeksToShow
  }

  const weeklyPercentageData = percentageData
    .filter(kpi => kpis.weekly.find(dailyKpi => dailyKpi.id === kpi.id))
    .map(kpi =>({
        ...kpi, 
        values:kpi.values.filter(d => d.date >= addWeeks(-(nrWeeksToShow.pre), injury.date))
      }))

  const weeklyData = addWeeklyData(player, weeklyPercentageData, weeklyOptions)
  weeklyData.info = {
    id:'weeklyTimeSeries',
    name:'Weekly Timeline',
    start:player.injury.date
  }
  console.log('weeklyData', weeklyData)

  //CHART CALLBACKS
  const onHoverWeek = weekNr => {
      setHoveredWeekNr(weekNr)
  }

  const onWeeklyCtrlClick = summary =>{
    setWeeklySummary(summary);
  }

  const weeklySettings = {
    valueKey:'rawValue',
    summary:weeklySummary,
    hoveredWeekNr:hoveredWeekNr,
    onHoverWeek:onHoverWeek,
    onWeeklyCtrlClick:onWeeklyCtrlClick
  };
  const dailySettings = {
    hoveredWeekNr:hoveredWeekNr
  };

	return (
		<div className={classes.root}>
      <section className={classes.topPanel}>
          <div className={classes.weeklyTrends}>
            <WeeklyTrends playerId={player.player_id} data={weeklyData} settings={weeklySettings}
              component={components.weekly[0]}/>
          </div>
          {weeklyData[0] && weeklyData[0].weeklyValues && <div className={classes.weeklyTable}>
              <ChronicAcutePlanner data={weeklyData} settings={weeklySettings}/>
          </div>}
      </section>
			<section className={classes.mainArea}>
          <div className={classes.dailyTitle}>Daily</div>
		    	<div className={classes.dailyComponents}>
		    		{components.daily.map(component =>
		    			<div className={classes[component.id]}
		    				key={'comp-'+component.id}>
		    				<DataComponent player={player} component={component} data={orderedDailyData}
                  settings={dailySettings}/>
		    			</div>)}
				  </div>
          <div className={classes.turns}>
            <div className={classes.gantts}>Turn-rates</div>
          </div>
			</section>
	    </div>
	)
}
Dashboard.propTypes = {
}
Dashboard.defaultProps = {
	sessions:[],
	player:{
    injury:{
      date:new Date()
    }
  },
  kpis:{},
  components:[]
}

export default Dashboard
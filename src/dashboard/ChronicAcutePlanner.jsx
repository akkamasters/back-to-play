import React, {useState, useEffect, useLayoutEffect}  from 'react';
import * as d3 from 'd3';
//material-ui
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
	root:{
		height:'100%',
		width:'100%',
    color:'black'
  },
  table:{
    height:'100%',
    width:'100%',
  },
  topRow:{
    display:'flex',
    height:40,
    fontSize:'11px',
  },
  kpiRow:{
    display:'flex',
    height:30,
    fontSize:'10px',
  },
  col1:{
    //width:100,
    width:'22%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none'
  },
  col1Top:{
    borderLeft:'none',
    borderTop:'none'
  },
  col2:{
    //width:70,
    width:'30%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none', borderLeft:'none'
  },
  col3:{
    //width:100,
    width:'48%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none', borderLeft:'none'
  },
  col3A:{
    //width:50,
    width:'16%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none', borderLeft:'none'
  },
  col3B:{
    //width:50,
    width:'16%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none', borderLeft:'none'
  },
  col3C:{
    //width:50,
    width:'16%',
    height:'100%',
    border:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
    borderTop:'none', borderLeft:'none'
  },
  colTop:{
      borderTop:'solid', borderColor:'#D3D3D3', borderWidth:'thin',
  },
  headerCell:{
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
  },
  cell:{
    padding:2,
    paddingLeft:5,
    display:'flex',
    justifyContent:'flex-start',
    alignItems:'center',
  },
  col1Cell:{
    paddingLeft:10
  },
  headerWithSublabels:{
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'space-between'
  },
  nextWeek:{
    flex:'50% 0 0'
  },
  sublabels:{
    flex:'50% 0 0',
    width:'100%',
    display:'flex',
    justifyContent:'center',
    fontSize:10
  },
  sublabel:{
    flex:'33.333% 0 0',
    //border:'solid'
  },
  tableTitle:{
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center',
    fontWeight:'bold',
    fontSize:'11px'
  }
}));

const ChronicAcutePlanner = ({player, data, settings}) =>{
  const { valueKey, summary, hoveredWeekNr } = settings;
  //helper
  const summaryValues = d => {
    const { mean, lower, upper, upper2 } = d.chronic[valueKey][summary];
    return {
      mean:mean ? mean.toFixed(1) : 'No data',
      lower:lower ? lower.toFixed(1) : 'No data',
      upper:upper ? upper.toFixed(1) : 'No data',
      upper2:upper2 ? upper2.toFixed(1) : 'No data'
    }
  }

  const getLastWeek = kpi => kpi.weeklyValues[kpi.weeklyValues.length-1];
  const weekToShow = kpi => hoveredWeekNr || getLastWeek(kpi);

	const classes = useStyles();
	return (
		<div className={classes.root}>
      <div className={classes.table}>
        <div className={classes.topRow}>
          <div className={classes.tableTitle +' '+classes.col1 +' '+classes.col1Top+' '+classes.headerCell}>
          </div>

          <div className={classes.col2 +' '+classes.colTop +' '+classes.headerWithSublabels}>
              <div className={classes.nextWeek +' '+classes.headerCell}>Prev 4 Wks</div>
              <div className={classes.sublabels}>
                  <div className={classes.sublabel +' '+classes.headerCell}>average</div>
              </div>
          </div>

          <div className={classes.col3 +' '+classes.colTop +' '+classes.headerWithSublabels}>
            <div className={classes.nextWeek +' '+classes.headerCell}>Next Week</div>
            <div className={classes.sublabels}>
                <div className={classes.sublabel +' '+classes.headerCell}>0.8</div>
                <div className={classes.sublabel +' '+classes.headerCell}>1.2</div>
                <div className={classes.sublabel +' '+classes.headerCell}>1.5</div>
            </div>
          </div>
        </div>
        {data.map(kpi => {
          const lastWeek = getLastWeek(kpi);
          //console.log('lastWeek', lastWeek)
          const values = summaryValues(lastWeek);
          return(
            <div className={classes.kpiRow} key={'row-'+kpi.id}>
              <div className={classes.col1 +' '+classes.cell + ' '+classes.col1Cell}>{kpi.shortName}</div>
              <div className={classes.col2 +' '+classes.cell}>{values.mean}</div>
              <div className={classes.col3A +' '+classes.cell}>{values.lower}</div>
              <div className={classes.col3B +' '+classes.cell}>{values.upper}</div>
              <div className={classes.col3C +' '+classes.cell}>{values.upper2}</div>
            </div>
          )})}
      </div>
    </div>
	)
}
ChronicAcutePlanner.propTypes = {
}
ChronicAcutePlanner.defaultProps = {
  data:[],
  settings:{
    valueKey:'value',
    summary:'total'
  }
}

export default ChronicAcutePlanner
import React, {useState, useEffect, useLayoutEffect}  from 'react';
import * as d3 from 'd3';
//data
//material-ui
import { makeStyles } from '@material-ui/core/styles';
//children
import Chart from '../charts/Chart';
//helpers
import { addWeeks } from '../../data/TimeHelpers'

const useStyles = makeStyles(theme => ({
	chartCont:{
		height: '100%',//props => props.chartsContHeight,
		display:'flex', //not needed
		flexDirection:'column', //not needed
    }
}));
const DataComponent = ({player, component, data, settings}) =>{
	//margins
	//!warning - if top and bottom changed then need to also chage in turn-rates class in Dashboard
	const defaultMargin = {left:30, right:30, top:40, bottom:30};
	var margin;
	switch(component.id){
		case 'stagesTables':{
			margin = {...defaultMargin, left:15, right:15};
			break;
		}
		case 'sparklines':{
			margin = {...defaultMargin, left:50, right:50};
			break;
		}
		default:{
			margin = defaultMargin;
		}
	}
	const props = {
		//for consistency, on non-svg component, margin is actually padding
		paddingTop: component.id === 'titles' ? margin.top : 0,
		paddingBottom: component.id === 'titles' ? margin.bottom : 0
	}
	const classes = useStyles(props);
	//helper to get all ds
	const allDs = (data, includeTargets=true) =>{
		const actualDs = data.map(m => m.values).reduce((a,b) =>[...a, ...b], []);
		const targetDs = includeTargets ? 
			data.map(m => m.targets).reduce((a,b) =>[...a, ...b], []) : [];
		return [...actualDs, ...targetDs];
	}
	//common bounds
	const commonPercentageBounds = data => {
		//note - does this include targets ? if so, they dont have dates so xMin and max may be undefined
		const allDsIncludingTargets = allDs(data);
		return {
			xMin:d3.min(allDsIncludingTargets, d => d.date) || player.injury.date,
			xMax:d3.max(allDsIncludingTargets, d => d.date) || addWeeks(12, player.injury.date),
			yMin:d3.min(allDsIncludingTargets, d => d.value) || 0,
			yMax:d3.max(allDsIncludingTargets, d => d.value) || 100
		}
	}

	//chart data
	var chartData;
	if(component.name === 'sparklines'){
		//add targets if sparklines - todo - sort out
		chartData = data
			.map(kpi =>({
				...kpi,
				targets:kpi.targets.map(t =>{
					//add text setting to raw target
					if(typeof(t.rawValue) === 'number'){
						return{
							...t,
							textDisplay:{
								location:'start', key:'rawValue'
							}
						}
					}return t;
				})
			}))
	}else{
		//map data so its a copy as we will be mutating it by adding info
		chartData = data.map(d => d);
	}
	//add component info to data
	chartData.info = component;

	//add daily-specific styles to settings
	const dailySettings = {
		...settings,
		styles:{
			strip:{
				background:{type:'alternate-extended', colours:['#F0F0F0', '#E8E8E8']}
			}
		}
	}

	var chartWrapper = {
		//for now, data is same for all charts
		chart:component.chart,
		data:chartData,
		margin:margin,
		//make a copy so not mutated 
		settings:dailySettings
	}
	//var updateChartSizes;
	switch(component.id){
		case 'rowTitles':{
			break;
		}
		case 'dotPlots':{
			chartWrapper.settings.targets = [{id:'90pc', name:'90%', value:90}];
			chartWrapper.settings.direction = 'horizontal';
			break;
		};
		case 'stagesTables':{
			break;
		};
		case 'sparklines':{
			chartWrapper.settings.bounds = commonPercentageBounds(chartData);
			chartWrapper.settings.lineSettings = {
				//overide default of 'value' to show rawvalue
				text:{
					start:'rawValue',
					end:'rawValue'
				}
			}
			break;
		};
		default:{
		};
	}

	return(
		<div className={classes.chartCont}>
			<Chart playerId={player.player_id} chartWrapper={chartWrapper} />
		</div>
	)
}

DataComponent.propTypes = {
}
DataComponent.defaultProps = {
	component:{},
	settings:{}
}

export default DataComponent
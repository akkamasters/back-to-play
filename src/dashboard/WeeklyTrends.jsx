import React, {useState, useEffect, useLayoutEffect}  from 'react';
import * as d3 from 'd3';
//material-ui
import { makeStyles } from '@material-ui/core/styles';
//children
import Chart from './charts/Chart';
//chart functions
import weeklyTimeSeries from './charts/weeklyTimeSeries/WeeklyTimeSeries';
//helpers

const useStyles = makeStyles(theme => ({
	root:{
		height:'100%'
    }
}));

const WeeklyTrends = ({playerId, component, data, settings}) =>{
	const classes = useStyles();
	var chartWrapper = {
		id:component.id,
		data:data,
		chart:component.chart,
		settings:{
			...settings,
			domains:{
				x:data.xDomain
			}
		},
		margin:{left:100, right:50, top:25, bottom:10},
	}

	return(
		<div className={classes.root}>
			<Chart playerId={playerId} chartWrapper={chartWrapper}/>
		</div>
	)
}
WeeklyTrends.propTypes = {
}
WeeklyTrends.defaultProps = {
	component:{}
}

export default WeeklyTrends
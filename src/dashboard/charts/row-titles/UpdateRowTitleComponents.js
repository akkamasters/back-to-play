import * as d3 from 'd3';
import * as fc from 'd3fc';
import { diffInWeeks , addWeeks, addDays, millisecondsToDays } from '../../../data/TimeHelpers';


const updateStrips = (svg, data=[], sizes, options={}) =>{
	const { width, height, chartWidth, chartHeight, margin, strip } = sizes;

	const { background } = options;
	const gTranslation = i => 'translate(0 ,' +(margin.top +i*strip.height)+')';
	//helpers
	const backgroundColour = i => {
		if(background && background.type && background.type.includes('alternate-extended')){
			if(i % 2 === 0){
				return background.colours[0];
			}
			//transpoarent if no 2nd colour
			return background.colours[1] || 'transparent';
		}else{
			return 'transparent';
		}
	}

	//bind
	const updatedStripG = svg.selectAll('g.stripG')
		.data(data, (kpi,i) => i);
	//exit
	updatedStripG.exit().remove();
	//enter
	const stripGEnter = updatedStripG.enter()
		.append('g')
			.attr('class', 'stripG')
			.attr('transform', (kpi,i) => {
				return gTranslation(i)
			})
			//merge()

	stripGEnter.append('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		//need to shift as parent g is not shifted at all - rect starts in top left of strip
		.attr('x', 0)
		.attr('y', 0)
		.attr('fill', (kpi,i) => backgroundColour(i))
	//text

	const nthWord = (phrase,n) => {
		return phrase.split(' ')[n-1];
		} 

	const wordTranslation = (kpi,n) =>{
		if(n == 1){
			if(nthWord(kpi.name, 2)){
				//has second word so 2 lines
				return 'translate(10,' +((strip.height/2) -5) +')';
			}else{
				//only 1 line
				return 'translate(10,' +(strip.height/2) +')';
			}
		}else{
			//must be second word
			return 'translate(10,' +((strip.height/2)+5) +')';
		}
	}

	//todo - avoid repeats of styling
	stripGEnter.append('text')
		.attr('class', 'titleText1 titleText')
			.attr('transform', kpi => wordTranslation(kpi, 1))
			.style('text-anchor', 'start')
			.style('dominant-baseline', 'middle')
			.style('font-size','10px')
			//.style('font-size','12px') //for pres
			.style('font-family', 'Arial, Helvetica, sans-serif')
			.style('opacity', 0.7)
			.text(kpi => nthWord(kpi.name, 1))

	stripGEnter.append('text')
		.attr('class', 'titleText2 titleText')
			.style('display', kpi => nthWord(kpi.name, 2) ? 'auto' :'none')
			.attr('transform', kpi => wordTranslation(kpi, 2))
			.style('text-anchor', 'start')
			.style('dominant-baseline', 'middle')
			.style('font-size','10px')
			//.style('font-size','12px')  //for pres
			.style('font-family', 'Arial, Helvetica, sans-serif')
			.style('opacity', 0.7)
			.text(kpi => nthWord(kpi.name, 2))
	

	//update
	updatedStripG.attr('transform', (kpi,i) => gTranslation(i))

	updatedStripG.select('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (kpi,i) => backgroundColour(i))

	updatedStripG.select('text.titleText1')
		.attr('transform', kpi => wordTranslation(kpi, 1))
		.text(kpi => nthWord(kpi.name,1))

	updatedStripG.select('text.titleText2')
		.style('display', kpi => nthWord(kpi.name, 2) ? 'auto' :'none')
		.attr('transform', kpi => wordTranslation(kpi, 2))
		.text(kpi => nthWord(kpi.name,2))
}











export { updateStrips }

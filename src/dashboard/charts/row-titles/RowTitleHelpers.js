import * as d3 from 'd3';

export const calcXScale = (sizes, options={}) => {
	const { chartHeight, chartWidth, margin } = sizes
	//domain
	var requiredDomain;
	if(options.domain){
		requiredDomain = options.domain;
	}else if(options.ds){
		const lowerX = d3.min(options.ds);
		const upperX = d3.max(options.ds);
		requiredDomain = [lowerX, upperX];
	}else{
		requiredDomain = [0,100];
	}
	//range
	const requiredRange = [margin.left, chartWidth + margin.left];
	//scale
	var scale;
	if(typeof requiredDomain[0] === 'number'){
		scale = d3.scaleLinear();
	}else{
		scale = d3.scaleTime();
	}
	return scale.domain(requiredDomain)
				.nice()
				.range(requiredRange)
}

export const calcYScale = (sizes, options={}) => {
	const { chartHeight, chartWidth, margin } = sizes
	//domain
	var requiredDomain;
	if(options.domain){
		requiredDomain = options.domain;
	}else if(options.ds){
		const lowerY = d3.min(options.ds);
		const upperY = d3.max(options.ds);
		requiredDomain = options.numberOrder === 'high-to-low' ? 
			[upperY, lowerY] : [lowerY, upperY];
	}else{
		requiredDomain = [0,100];
	}
	//range
	var requiredRange;
	if(options.range){
		requiredRange = options.range;
	}else{
		requiredRange = [chartHeight + margin.top, margin.top];
	}
	//scale
	return d3.scaleLinear()
			.domain(requiredDomain)
			.nice()
			.range(requiredRange)
}

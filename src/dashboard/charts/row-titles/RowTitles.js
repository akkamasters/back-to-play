import * as d3 from 'd3';
import { flattenDs } from '../../../data/DataHelpers';
import { diffInWeeks , addWeeks, addDays } from '../../../data/TimeHelpers';
//time-sries
import { updateStrips } from './UpdateRowTitleComponents';
import { updateTitle } from '../UpdateCommonComponents';

/**
* 
* 
*/
function rowTitles(){
	//flags and identifiers
	var svg, chartG;
	//setings
	var sizes = {
		isDefault:true,
		//default is MS sizes
		width:500, height:200,
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	var styles ={
		strip:{
		}
	}
	//allds is a useful array of every d
	var data;
	//api-driven update handlers
	var updateSizes;
	var mainUpdate;
	//dom elements
	//var updateElements
	var onHoverKpi;
	const dispatcher = d3.dispatch('kpiHover');

	function chart(selection){
		updateSizes = function(){
			mainUpdate();
		}
		mainUpdate = function(){
			//set strip sizes
			const stripHeight = sizes.chartHeight/data.length;
			const stripMargin = {top:10, bottom:5, left:0, right:0};
			sizes.strip = {
				height:stripHeight,
				width:sizes.width,
				margin:stripMargin,
				chartHeight: stripHeight - stripMargin.top - stripMargin.bottom,
				chartWidth: sizes.chartWidth - stripMargin.left - stripMargin.right
			}
			
			updateStrips(svg, data, sizes, {background:styles.strip.background})
		}
		
		onHoverKpi = function(){
		}
		selection.each(function(chartData){
			data = chartData;
			if(!svg || svg.select('g').node().length === 0){
				svg = d3.select(this);
			}
			//init updates
			mainUpdate();
		})
	}
	//api
	//object sizes
	chart.sizes = function(value){
		if(!arguments.length)
			return sizes;
		sizes = {...sizes, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSizes === 'function')
			updateSizes();
		return chart;
	}
	//styles
	chart.styles = function(value){
		if(!arguments.length)
			return styles;
		styles = {...styles, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateStyles === 'function'){
			updateStyles();
		}
		return chart;
	}
	//initial return of chart to be called on an element with data
	return chart;
}

export default rowTitles;
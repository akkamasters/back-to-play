
//note: margin.top may be different for components, so title will be
//displayed at top of margin
const updateTitle = (svg, sizes, title='', options={}) =>{
	const { margin, chartWidth, chartHeight } = sizes;
	const { transform, textAnchor, dominantBaseline, fontSize, fontWeight, fontFamily} = options;
	const titleG = svg.select('g.titleG')
		.attr('transform',transform || 'translate('+(margin.left+chartWidth*0.5) +','+0 +')')

	titleG.select('text.titleText')
		.style('text-anchor', textAnchor || 'middle')
		//.style('alignment-baseline', 'hanging')
		.style('dominant-baseline', dominantBaseline || 'hanging')
		.style('font-size',fontSize || '12px')
		//.style('font-size','14px') //for pres
		.attr('font-weight', fontWeight || 'lighter')
		.attr('font-family', fontFamily || 'Arial, Helvetica, sans-serif')
		.text(title)
}

const updateNoDataMesg = (svg, noData, mesg) =>{
	if(noData){
		svg.select('text.noDataText')
			.style('display', 'initial')
			.attr('transform', 'translate(20,20)')
			.attr('stroke', 'black')
			.text(mesg)
	}else{
		svg.select('text.noDataText').style('display', 'none')
	}
}

export { updateTitle, updateNoDataMesg }
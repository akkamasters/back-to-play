 import React, {useState, useEffect, useLayoutEffect}  from 'react'
//import PropTypes from 'prop-types'
import * as d3 from 'd3';
import debounce from 'lodash/debounce';
import throttle from 'lodash/throttle';
//material-ui
import { makeStyles } from '@material-ui/core/styles';
//helpers

const useStyles = makeStyles(theme => ({
	root:{
		width:'100%',
		height:'100%',
  		//border:'solid', borderColor:'red', borderWidth:'thin'
    },
    svg:{
    	//border:'solid', borderColor:'blue', borderWidth:'thin'
    },
    dNone:{
    	display:'none'
    },
    mesg:{
    	margin:theme.spacing(4)
    },
    loading:{
    	color:'white'
    },
    error:{
    	color:'red'
    }
}));

const Chart = ({playerId, chartWrapper, updateChartSizes, dataStatus, direction}) =>{
	const classes = useStyles();
	const { hoveredWeekNr, summary } = chartWrapper.settings;
	useLayoutEffect(() => {
		if(dataStatus === 'loaded'){
			const id = chartWrapper.data.info.id || '';
			const svg = d3.select('svg#svg-'+id);
			const chartRendered = svg.select('g').size() > 0;
			//defaults
			if(!chartWrapper.settings){
				chartWrapper.settings = {};
			}
			const render = chartWrapper.render || renderChart;
			const update = chartWrapper.update || updateChart;
			//console.log('Chart', chartWrapper.data.info.id)
			//console.log('chartRendered?', chartRendered)
			if(!chartRendered){
				//set init sizes
				//console.log('init sizes')
				const cont = d3.select('div#svg-cont-'+id).node();
				const contWidth = cont.getBoundingClientRect().width;
				//An extra 1vh was subtracted when create svgheight variable
				const contHeight = cont.getBoundingClientRect().height;
				//set svg
				d3.select('svg#svg-'+id).attr('height', ''+contHeight+'px');
				//d3.select('svg#svg-'+id).attr('width', ''+contWidth+'px');
				//need to update sizes for this chart
				const chartSizes = {
					contHeight:contHeight,
					contWidth:contWidth,
					...updateChartSizes(contWidth, contHeight, chartWrapper.margin)
				}
				//update sizes in settings
				chartWrapper.settings.sizes = chartSizes
			}

			//always re-render if new data (for now)
			//if(chartNotRendered || chartWrapper.data){
			render(chartWrapper, svg);
			//}else{
				//update(chartWrapper);
			//}
		}
	}, [playerId, hoveredWeekNr, summary])
	useEffect(function setupListener() {
	    function handleResize() {
	    	//no need fro setTIMEOUT we can see from teh svg border that its got teh correct dimensions.
    		//console.log('resize...')
    		const id = chartWrapper.data.info.id || '';
    		const svg = d3.select('svg#svg-'+id);
			const chartRendered = svg.select('g').size() > 0;
	  		if(chartRendered){
	  			const cont = d3.select('div#svg-cont-'+id).node();
		  		const contWidth = cont.getBoundingClientRect().width;
		  		//console.log('contWidth', contWidth)
				const contHeight = cont.getBoundingClientRect().height;
				//set svg height
				d3.select('svg#svg-'+id).attr('height', ''+contHeight+'px');
				//d3.select('svg#svg-'+id).attr('width', ''+contWidth+'px');
				const newChartSizes = {
					contHeight:contHeight,
					contWidth:contWidth,
					...updateChartSizes(contWidth, contHeight, chartWrapper.margin)
				}
				//update chart
				chartWrapper.chart.sizes(newChartSizes);
			}
	   	}

	    window.addEventListener('resize', debounce(handleResize, 100))
		/*
	    window.addEventListener('resize', debounce(function() {
	    	console.log('resize', new Date())
    		//need to update sizes for this chart
			const id = chartWrapper.data.info.id || '';
			const cont = d3.select('div#svg-cont-'+id).node();
	  		if(cont){
	  			const contWidth = cont.getBoundingClientRect().width;
				//An extra 1vh was subtracted when create svgheight variable
				const contHeight = cont.getBoundingClientRect().height;
				//set svg
				d3.select('svg#svg-'+id).attr('height', ''+contHeight+'px');
				//d3.select('svg#svg-'+id).attr('width', ''+contWidth+'px');
				const newChartSizes = {
					contHeight:contHeight,
					contWidth:contWidth,
					...updateChartSizes(contWidth, contHeight, chartWrapper.margin)
				}
				//update state
				handleSizeUpdate(id, newChartSizes);
				//update chart
				chartWrapper.chart.sizes(newChartSizes);
	   	 	}


	    }, 500));
	    */


	    return function cleanupListener() {
	      window.removeEventListener('resize', handleResize)
	    }
	})

   	const id = chartWrapper.data.info.id || '';

   	//about height:
   	//in this app, we can reliably say what the height will be in terms of vh, because we only
   	//have two components - weekly and daily. Perhaps the dimensions will change depending on 
   	//nr of kpis, but that can be coded for. Also, in the long run, the solution is to 
   	//move to using viewbox so it automatically fills the space.
   	//In fact, for temp measure, even if height is too big (ie default:1000 in defaultprops}, it doesnt matter as overflow
   	//doesnt affect the component below as long as there is nothing rendered in the space.
	return (
		<div className={classes.root} id={'svg-cont-'+id}>
			{dataStatus === 'loaded' ?
				<svg id={'svg-'+id} 
					className={classes.svg} width='100%' >
					<filter id='blur'>
					   <feGaussianBlur in='SourceGraphic' stdDeviation='1.5'/>
					</filter>
				</svg>
				:
			    <div className={classes.mesg}>
			    	{dataStatus === 'loading' ? 
			    		<div className={classes.loading}>Data loading...</div>
			    		:
			    		<div className={classes.error}>Data error</div>
			    	}
			    </div>
			}
	    </div>
	)
}
Chart.propTypes = {
}
Chart.defaultProps = {
	chart:{},
	classes:{},
	dataStatus:'loaded',
	direction:'column',
	updateChartSizes:function(width=500, height=400, adjustedMargin={}){
		const margin = {top:60, bottom:40, left:30, right:30, ...adjustedMargin}
		return {
			width:width,
			height:height,
			chartWidth:width - margin.left - margin.right,
			chartHeight:height - margin.top - margin.bottom,
			margin:margin
		}
	}
}
//DEFAULT CHART FUNCTIONS
//default render if no custom render exists
function renderChart(chartWrapper, svg){
	//console.log('renderChart', chartWrapper.settings.sizes.margin)
	const { chart, data } = chartWrapper;
	if(chart && data){
		const settings = chartWrapper.settings || {};

		if(settings.sizes){
			chart.sizes(settings.sizes);
		}
		if(settings.targets){
			chart.targets(settings.targets);
		}
		if(settings.bounds){
			chart.bounds(settings.bounds);
		}
		if(settings.valueKey){
			chart.valueKey(settings.valueKey);
		}
		if(settings.summary){
			chart.summary(settings.summary);
		}
		if(settings.keys){
			chart.keys(settings.keys);
		}
		if(settings.lineSettings){
			chart.lineSettings(settings.lineSettings);
		}
		if(typeof chart.hoveredWeekNr === 'function' 
			&& (settings.hoveredWeekNr || settings.hoveredWeekNr === '')){
			chart.hoveredWeekNr(settings.hoveredWeekNr);
		}
		if(chartWrapper.data.info.id == 'weeklyTimeSeries'){
			chart.on('verticalStripHover', settings.onHoverWeek);
		}
		if(chartWrapper.data.info.id == 'weeklyTimeSeries'){
			chart.onCtrlClick('weeklyCtrlClick', settings.onWeeklyCtrlClick);
		}
		if(settings.styles){
			chart.styles(settings.styles);
		}
		svg.datum(data).call(chart);
	}
}
//default render if no custom updateFunc exists
function updateChart(chartWrapper){
	//console.log('updateChart', chart)
	const { sizes, chart} = chartWrapper;
	if(sizes){
		chart.sizes(sizes);
	}
}

/*
function updateChartSizesDefault(width, height, settings={}){
	var chartWidth, chartHeight;
	var margin = {};
	//max chart dimns are width=640 and height =450
	if(height >= 500){
		chartHeight = 400;
		margin.top = (height - chartHeight)*0.5;
		margin.bottom = (height - chartHeight)*0.5;
	}else{
		margin.top = d3.max([height * 0.2, 60]);
		margin.bottom = d3.max([height * 0.2, 60]);
		chartHeight = height - margin.top - margin.bottom;
	}

	if(width >= 850){
		chartWidth = 500;
		margin.left = (width - chartWidth)*0.5;
		margin.right = (width - chartWidth)*0.5;
	}else{
		const minSideMargin = 100;
		const maxSideMargin = width *0.1;
		const sideMargin = Math.max(minSideMargin, maxSideMargin);
		margin.left = sideMargin;
		//margin.right = sideMargin;
		margin.right = 30;
		chartWidth = width - margin.left - margin.right;
	}
	return {
		width:width, height:height, margin:margin,
		chartWidth:chartWidth, chartHeight:chartHeight
	};
}
*/



export default Chart
import * as d3 from 'd3';
import { flattenDs } from '../../../data/DataHelpers';
import { diffInWeeks , addWeeks, addDays } from '../../../data/TimeHelpers';
//time-sries
import { calcXScale, calcYScale } from './SparklineHelpers';
import { updateStrips } from './UpdateSparklineComponents';
import { updateTitle } from '../UpdateCommonComponents';

/**
* 
* 
*/
function sparklines(){
	//flags and identifiers
	var svg, chartG;
	//setings
	var sizes = {
		isDefault:true,
		//default is MS sizes
		width:500, height:200,
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	var styles ={
		strip:{
		}
	}
	//allds is a useful array of every d
	var data;
	var bounds = {};
	var scales = {};
	var pointsToShow = {
		weeklyMax:2,//undefined,
		nrWeeks:undefined,
		max:undefined //so 4 weeks
	}
	var hoveredWeekNr = '';
	var axesToShow=[];
	var labelsToShow = [];
	var lineSettings = {
		type:'straight',
		text:{
			start:'value',
			end:'value'
		}
	}
	var withMovingAvg = true;
	//api-driven update handlers
	var updateSizes;
	var mainUpdate, updateScales;
	//dom elements
	//var updateElements
	var onHoverDataLine;

	const dispatcher = d3.dispatch('dataLineHover');

	function chart(selection){
		updateSizes = function(){
			mainUpdate();
		}
		mainUpdate = function(){
			//set strip sizes
			const stripHeight = sizes.chartHeight/data.length;
			const stripMargin = {top:10, bottom:5, left:0, right:0};
			sizes.strip = {
				height:stripHeight,
				width:sizes.width,
				margin:stripMargin,
				chartHeight: stripHeight - stripMargin.top - stripMargin.bottom,
				chartWidth: sizes.chartWidth - stripMargin.left - stripMargin.right
			}
			updateScales();
			//updateElements();

			const dataToShow = data.map(kpi =>{
				var valuesToShow = kpi.values;
				if(pointsToShow.weeklyMax){
					//filter ds for each week so only top two
					valuesToShow = valuesToShow.filter((d,i) =>{
						//todo - change .value to using valueKey object
						const topTwo = valuesToShow.filter(d2 => d2.weekNr === d.weekNr)
												   .sort((d1,d2) => d2.value - d1.value)
												   .slice(0,2)

						return topTwo.find(d2 => d2.date === d.date);
					})
				}
				const sortedValues = valuesToShow.sort((v1,v2) => v2.date - v1.date);

				return{
					...kpi,
					values:sortedValues
				}
			})
			const stripOptions = {
				axesToShow:axesToShow,
				labelsToShow:labelsToShow,
				background:styles.strip.background,
				hoveredWeekNr:hoveredWeekNr
			}
			updateStrips(svg, data, sizes, scales, stripOptions)
			//updateTitle(svg, sizes, data.info.name)
		}
		updateScales = function(updates){
			scales.x = calcXScale(sizes, {domain:[bounds.xMin, bounds.xMax]});
			scales.y = calcYScale(sizes.strip, {domain:[bounds.yMin, bounds.yMax]});
			/*scales.ys = data.map((kpi,i) => {
				//bounds have been determined centrally so same for all sparklines
				const scale =  calcYScale(sizes.strip, {domain:[bounds.yMin, bounds.yMax]});
				console.log('kpi', kpi.id)
			console.log('y dom', scale.domain())
			console.log('y range', scale.range())
			return scale;
			})
			*/
		}
		/*updateElements = function(){
			updateDataLines('', svg, data.kpis, scales, lineSettings);
			updateMovingAvgLines(svg, data.kpis, scales, withMovingAvg)
			//flatten targets from all kpis
			const targets = data.kpis
				.map(kpi => kpi.targets.map((t,i) =>({
						...t,
						id:kpi.id+'-'+i,
						kpiId:kpi.id
					}))
				).reduce((a,b) =>[...a,...b],[])
			updateTargetLines(svg, targets, scales.y, sizes)
			//ADDED
			//this attaches any start values to a line

			if(axesToShow.x){
				updateXAxis({nrTicks:5});
			}
			if(axesToShow.y){
				updateYAxis({nrTicks:5});
				//mini y label - units only
				updateYLabel();
			}
		}*/
		onHoverDataLine = function(){
		}
		selection.each(function(chartData){
			//console.log('sparklines data', chartData)
			//note - targets can either have a value, or values
			/*data = {
				...chartData,
				kpis:chartData.kpis.map(kpi => ({
					...kpi,
					targets:kpi.targets.map(t =>{
						if(t.values){
							//no need to change
							return t;
						}
						return{
							//create separet target value for each kpi value
							//same as target.value (so its a horizontal line)
							...t,
							values:kpi.values.map(v => t.value)
						}

					})
				}))
			}
			*/
			data = chartData;
			if(!svg || svg.select('g').node().length === 0){
				svg = d3.select(this);
				svg.append('g').attr('class', 'titleG');
				svg.select('g.titleG').append('text').attr('class', 'titleText');
			}
			//init updates
			mainUpdate();

			//hover handler
			dispatcher.on("dataLineHover", function(kpiId){
				if(withPoints == 'none'){
					onHoverDataLine(kpiId);
				}
			});
		})
	}
	//api
	//object sizes
	chart.sizes = function(value){
		if(!arguments.length)
			return sizes;
		sizes = {...sizes, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSizes === 'function')
			updateSizes();
		return chart;
	}
	//styles
	chart.styles = function(value){
		if(!arguments.length)
			return styles;
		styles = {...styles, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateStyles === 'function'){
			updateStyles();
		}
		return chart;
	}
	//object bounds
	chart.bounds = function(value){
		if(!arguments.length)
			return bounds;
		if(value !== 'no-change')
			bounds = {...bounds, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateBounds === 'function')
			updateBounds(bounds);
		return chart;
	}
	chart.lineSettings = function(value){
		if(!arguments.length)
			return lineSettings;
		if(value !== 'no-change')
			lineSettings = {...lineSettings, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateLineSettings === 'function')
			updateLineSettings(lineSettings);
		return chart;
	}
	//number hoveredWeekNr
	chart.hoveredWeekNr = function(value){
		if(typeof arguments.length !== 'number' && value !== ''){
			return hoveredWeekNr;
		}
		hoveredWeekNr = value;
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateHoveredWeekNr === 'function')
			updateHoveredWeekNr();
		return chart;
	}

	//initial return of chart to be called on an element with data
	return chart;
}

export default sparklines;
import * as d3 from 'd3';
import * as fc from 'd3fc';
import { diffInWeeks , addWeeks, addDays, millisecondsToDays } from '../../../data/TimeHelpers';
/**
*
*	
**/
//NOTE - TODO - CHANGE FUNCTION NAMES AS ITS ONE LINE, AND THEN HAVE LINE SEGMENTS
//THAT MAKE UP THE LINE
const updateDataLines = (stripG, kpi, scales, options) =>{
	const { x, y } = scales;
	const { hoveredWeekNr } = options;
		
	const strokeWidth = (d,i) =>{
		//gets the prev d ie i-1 and calcs gradient
		if(hoveredWeekNr){
			if(d.weekNr === hoveredWeekNr){
				return 3;
			}else{
				return 0.8
			}
		}
		return 1.2;
	}
	
	const strokeColour = (d,i) =>{
		//gets the prev d ie i-1 and calcs gradient
		if(d.weekNr === hoveredWeekNr){
			//return '#708090'
			return '#ffa343'
			//return '#3A424A'
		}
		return 'black';
	}

	//1.BIND
	//Note: stripG is positioned at 0,0, so its just a conceptual container for each kpi
	//translations are built into the scales, with a separate yScale for each strip
	const updatedDataLine = stripG.selectAll('line.dataLine')
		.data(kpi.values, (d,i) => (d.id ? d.id : i));
	//2.EXIT
	updatedDataLine.exit().remove();
	//3.ENTER
	const dataLineEnter = updatedDataLine.enter()
		.append('line')
			.style('display', (d,i) => i === kpi.values.length-1 ? 'none':'auto')
			.attr('class', 'dataLine')
			.attr('x1', d => x(d.date))
			.attr('x2', (d,i) => i === kpi.values.length-1 ? 0 : x(kpi.values[i+1].date))
			.attr('y1', d => {
				//console.log('entering d for dataline', d)
				return y(d.value)
			})
			.attr('y2', (d,i) => i === kpi.values.length-1 ? 0 : y(kpi.values[i+1].value))
			.attr("fill", "none")
			.attr("stroke", (d,i) => strokeColour(d,i))
			.attr("stroke-width", (d,i) => strokeWidth(d,i))

	//4.UPDATE
	updatedDataLine
		.attr('x1', d => x(d.date))
		.attr('x2', (d,i) => i === kpi.values.length-1 ? 0 : x(kpi.values[i+1].date))
		.attr('y1', d => y(d.value))
		.attr('y2', (d,i) => i === kpi.values.length-1 ? 0 : y(kpi.values[i+1].value))
		.attr("stroke", (d,i) => strokeColour(d,i))
		.attr("stroke-width", (d,i) => strokeWidth(d,i))


}

const updateDataText = (stripG, scales, options) =>{
	const { x, y } = scales;
	//TEXT
	//TODO - if peaktext is within 20px of end of xRange, make text-anchor = start
	//helpers
	const startD = kpi => {
		const x1 = d3.min(kpi.values, v => v.date);
		return kpi.values.find(v => v.date === x1);
	}
	const endD = kpi => {
		const xEnd = d3.max(kpi.values, v => v.date);
		return kpi.values.find(v => v.date === xEnd);
	}
	const peakD = kpi => {
		const peakValue = d3.max(kpi.values, v => v.value);
		return kpi.values.find(v => v.value === peakValue);
	}

	const startTextTranslation = kpi => {
		const start = startD(kpi)
		if(start){
			return 'translate(' +(x(start.date) - 3) +',' +y(start.value) +')';
		}else{
			//no ds so display none
			return 'translate(0,0)'
		}
	}
	const endTextTranslation = kpi => {
		const end = endD(kpi);
		if(end){
			return 'translate(' +(x(end.date) + 3) +',' +y(end.value) +')';
		}else{
			//no ds so display none
			return 'translate(0,0)';
		}
	}

	const peakTextTranslation = kpi => {
		const peak = peakD(kpi);
		if(peak){
			return 'translate(' +(x(peak.date)) +',' +(y(peak.value) - 6) +')';
		}else{
			//no ds so display none
			return 'translate(0,0)';
		}
	}
	stripG.select('text.startText')
		.attr('transform', kpi => startTextTranslation(kpi))
		.style('opacity', 0.8)
		.text(kpi => kpi.values.length > 0 ? startD(kpi).rawValue : '')

	stripG.select('text.endText')
		.attr('transform', (kpi,i) => endTextTranslation(kpi))
		.style('opacity', 0.8)
		.text(kpi => kpi.values.length > 1 ? endD(kpi).rawValue : '')

	stripG.select('text.peakText')
		.attr('transform', (kpi,i) => peakTextTranslation(kpi))
		.style('opacity', 0.8)
		.text(kpi => (kpi.values.length > 1 && peakD(kpi).date !== endD(kpi).date) ? 
			peakD(kpi).rawValue : '')
}



const updateMovingAvgLines = (stripG, kpi, scales, options={}) =>{
	const { x, y } = scales;
	const { show } = options;
	const strokeWidth = (d,i) =>{
		//gets the prev d ie i-1 and calcs gradient
		return 2;
	}
	
	const strokeColour = (d,i) =>{
		//gets the prev d ie i-1 and calcs gradient
		//return 'orange';
		//return '#00AFAA'; //teal
		return '#00dbd4' //25% lighter
	}
	const showSegment = i =>{
		//show if show==true, there are more than 2 pts and its not the last point
		return show && kpi.values.length >= 3 && i !== kpi.values.length-1
	}

	const sameAsDataLine = (d,i) =>{
		if(i === kpi.values.length-1){
			return false;
		}
		const nextD = kpi.values[i+1];
		return  d.movingAvg === d.value && nextD.movingAvg === nextD.value;
	}
	//shift line up if exactly same pos and direction as dataline
	const y1 = (d,i) =>{
		const shift = sameAsDataLine(d,i) ? 3 : 0;
		return y(d.movingAvg) - shift;
	}
	const y2 = (d,i) =>{
		if(i === kpi.values.length-1){
			return 0;
		}
		const shift = sameAsDataLine(d,i) ? 3 : 0;
		return y(kpi.values[i+1].movingAvg) - shift;
	}

	//1.BIND
	//Note: stripG is just a conceptual container
	const updatedMovingAvgSegment = stripG.selectAll('line.movingAvgSegment')
		.data(kpi.values, (d,i) => (d.id ? d.id : i));
	//2.EXIT
	updatedMovingAvgSegment.exit().remove();
	//3.ENTER
	const movingAvgSegmentEnter = updatedMovingAvgSegment.enter()
		.append('line')
			.style('display', (d,i) => showSegment(i) ? 'auto':'none')
			.attr('class', 'movingAvgSegment')
			.attr('x1', d => x(d.date))
			.attr('x2', (d,i) => i === kpi.values.length-1 ? 0 : x(kpi.values[i+1].date))
			.attr('y1', (d,i) => y1(d,i))
			.attr('y2', (d,i) => y2(d,i))
			.attr("fill", "none")
			.attr("stroke", (d,i) => strokeColour(d,i))
			.attr("stroke-width", (d,i) => strokeWidth(d,i))

	updatedMovingAvgSegment
		.style('display', (d,i) => showSegment(i) ? 'auto' : 'none')
		.attr('x1', d => x(d.date))
		.attr('x2', (d,i) => i === kpi.values.length-1 ? 0 : x(kpi.values[i+1].date))
		.attr('y1', (d,i) => y1(d,i))
		.attr('y2', (d,i) => y2(d,i))

}



const updateStrips = (svg, data=[], sizes, scales, options={}) =>{
//issues
//1. startText is not in correct pos for Nick pope
//2. target text needs to be updated properly


	const { width, height, chartWidth, chartHeight, margin, strip } = sizes;
	const { x, y } = scales;
	const { background, hoveredWeekNr } = options;
	const gTranslation = i => 'translate(0 ,' +(margin.top +i*strip.height)+')';
	//helpers
	const backgroundColour = i => {
		if(background.type && background.type.includes('alternate-extended')){
			if(i % 2 === 0){
				return background.colours[0];
			}
			//transpoarent if no 2nd colour
			return background.colours[1] || 'transparent';
		}else{
			return 'transparent';
		}
	}



	//bind
	const updatedStripG = svg.selectAll('g.stripG')
		.data(data, (kpi,i) => i)

	//exit
	updatedStripG.exit().remove();
	//enter
	const stripGEnter = updatedStripG.enter()
		.append('g')
			.attr('class', 'stripG')
			.attr('transform', (kpi,i) => gTranslation(i))
			//merge()
	//ELEMENTS
	//background rects
	stripGEnter.append('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		//need to shift as parent g is not shifted at all - rect starts in top left of strip
		.attr('x', 0)
		.attr('y', 0)
		.attr('fill', (kpi,i) => backgroundColour(i))

	//update
	updatedStripG.attr('transform', (kpi,i) => gTranslation(i))

	updatedStripG.select('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (kpi,i) => backgroundColour(i))


	//datapoints - use each as its a more complex function for each stripG ie several datapoints
	//update ds for any newly entered strips
	//todo - think abot refactoring because I am refering to stripG in both loops but in 2nd loop it is 
	//oinly the updated strips, so hw shoul dtheze be processed differently?
	stripGEnter.each(function(kpi,i){
		const enteredStripG = d3.select(this);
		//STATIC RENDERS
		enteredStripG.append('text').attr('class', 'startText')
			.style('text-anchor', 'end')
			//.style('alignment-baseline', 'middle')
			.style('dominant-baseline', 'middle')
			.style('font-size','10px')

		enteredStripG.append('text').attr('class', 'endText')
			.style('text-anchor', 'start')
			//.style('alignment-baseline', 'middle')
			.style('dominant-baseline', 'middle')
			.style('font-size','10px')

		enteredStripG.append('text').attr('class', 'peakText')
			.style('text-anchor', 'middle')
			//.style('alignment-baseline', 'middle')
			.style('dominant-baseline', 'middle')
			.style('font-size','10px')

		//DYNAMIC RENDERS AND UPDATES
		//same y scale for all strips, just g starts in different pos
		//console.log('options', options)
		updateMovingAvgLines(enteredStripG, kpi, scales, {...options, show:true});
		updateDataLines(enteredStripG, kpi, scales, options);
		//targs
		updateTargetGs(enteredStripG, kpi, scales, options);
		updateDataText(enteredStripG, scales, options)
	})
	//update ds for any updatedStrips
	updatedStripG.each(function(kpi,i){
		const updatedStripG = d3.select(this);
		//console.log('update options', options)
		//one chart line is made up of lots of little lines
		//WARNING - TODO - DUE TO EUE PATTERN, A MOVAVG LINE MAY STILL BE RENDERED
		//AFTER A DATALINE
		updateMovingAvgLines(updatedStripG, kpi, scales, {...options, show:true});
		updateDataLines(updatedStripG, kpi, scales, options)
		updateTargetGs(updatedStripG, kpi, scales, options);
		updateDataText(updatedStripG, scales, options)
	})
}





const updateTargetGs = (stripG, kpi, scales, options) =>{
	const { x, y } = scales;
	//helpers
	const targetTextTranslation = t => 'translate(' +(x.range()[0] - 3) +',' +y(t.value)+')';
	//qs - is it right fro this to apply to updatedStrips too - I think so yer, 
	//its just that the target lines will already be there so
	//but it doesnt make sense as it is...................
	//g is a conceptual container, no translation

	//1.BIND
	const updatedTargetG = stripG.selectAll('g.targetG')
		.data(kpi.targets, (t,i) => i)
	//2.EXIT
	updatedTargetG.exit().remove();

	//3.ENTER
	const targetGEnter = updatedTargetG.enter()
		.append('g').attr('class', 'targetG')

	targetGEnter.append('line')
		.attr('class', 'targetLine')
		.attr('x1', x.range()[0]) //x same for all strips
		.attr('x2', x.range()[1])
		.attr('y1', t => {
			return  y(t.value);
		})
		.attr('y2', t => {
			return y(t.value);
		})
		.attr("fill", "none")
		.attr("stroke", 'black')//(d,i) => strokeColour(d,i))
		.attr("stroke-width", 0.7)//(d,i) => strokeWidth(d,i))
		.style('stroke-dasharray', '4')

	targetGEnter.append('text')
		.attr('class', 'targetText')
		.attr('transform', t => targetTextTranslation(t))
		.style('text-anchor', 'end')
		//.style('alignment-baseline', 'middle')
		.style('dominant-baseline', 'middle')
		.style('font-size','11px')
		.style('font-weight', 'bold')
		.style('opacity', 0.8)
		.text(t => 'T '+t.rawValue)

	//4.UPDATE

	updatedTargetG.select('line.targetLine')
		.attr('x1',  x.range()[0]) //x same for all strips
		.attr('x2', x.range()[1])
		.attr('y1', t => {
			return y(t.value);
		})
		.attr('y2', t => {
			return y(t.value);
		})

	updatedTargetG.select('text.targetText')
		.attr('transform', t => targetTextTranslation(t))
		.text(t => 'T '+t.rawValue)


}





const updateXAxis = (selection, scale, sizes, interval) =>{
	/*
	example to get equal ticks of a specific interval size
	var step = 5,
    min = 20,
    max = 170,
    stepValue = (max - min) / (step - 1),
    tickValues = d3.range(min, max + stepValue, stepValue);*/

	const gapInDays = millisecondsToDays(scale.domain()[1] - scale.domain()[0]);
	//console.log('gap in days', gapInDays)
	if(!interval){
		interval = gapInDays < 21 ? 'day' : gapInDays < 112 ? 'week' : 'month';
	}
	var d3TimeInterval;
	switch(interval){
		case 'second':{
			d3TimeInterval = d3.timeSecond;
			break;
		}
		case 'minute':{
			d3TimeInterval = d3.timeMinute;
			break;
		}
		case 'day':{
			d3TimeInterval = d3.timeDay;
			break;
		}
		case 'week':{
			d3TimeInterval = d3.timeWeek;
			break;
		}
		case 'month':{
			d3TimeInterval = d3.timeMonth;
			break;
		}
		case 'year':{
			d3TimeInterval = d3.timeWeek;
			break;
		}
		default:{
			d3TimeInterval = d3.timeDay;
		}
	}

	const axis = fc.axisLabelRotate(fc.axisBottom(scale))
		.ticks(d3TimeInterval)
		.tickFormat(d3.timeFormat("%d-%m"))
		//.tickValues(tickValues)
		//.tickFormat(d3.format("d"))
		.labelRotate(45)
		.decorate(sel =>{
			sel.enter().selectAll("path")
				.style("stroke-width", 0.6)
				.style("stroke", "black")
				//.style("stroke", "white") 
				//.style("stroke", '#f8f9fa')
				.style("opacity", 0.6)

			sel.enter().selectAll("text")
				.style("stroke-width", 0.6)
				.style("stroke", "black")
				//.style("stroke", "white")
				//.style("stroke", '#f8f9fa')
				.style("opacity", 0.6)
		})
		selection.select('g.xAxisG')
			//.transition(d3.transition().duration(500))
			.attr("transform", `translate(0, ${sizes.margin.top +sizes.chartHeight})`)

		selection.select('g.xAxisG').call(axis)

		selection.select('g.xAxisG').select(".domain")
			.style("stroke-width", 0.6)
			.style("stroke", "black")
			.style("opacity", 0.6)
			
}

/**
*
*
**/
const updateYAxis = (selection, scale, sizes, visibility) =>{
	const axis = d3.axisLeft().scale(scale).ticks(5)
	selection.select("g.yAxisG")
		.attr("transform", 'translate(' +sizes.margin.left +',' +0 +')')
		.style('display', visibility == 'hidden' ? 'none' : 'initial')
		.call(axis)

	selection.select('g.yAxisG').selectAll("line")
		.style("stroke-width", 0.6)
		.style("stroke", "black")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", 0.6);
	selection.select('g.yAxisG').selectAll("path")
		.style("stroke-width", 0.6)
		.style("stroke", "black")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", 0.6);
	selection.select('g.yAxisG').selectAll("text")
		.style("stroke-width", 0.6)
		.style("stroke", "black")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", 0.6);
}

const updateYLabel = (selection, position, sizes, line='', visibility) =>{
	const { margin, chartWidth, chartHeight } = sizes;
	const labelGap = 50;
	const horizShift = margin.left - labelGap;
	const vertShift = sizes.margin.top + chartHeight *0.5;
	//line 1
	const lineSelector = position === 'left' ? 'text.yLabel' : 'text.extraYLabel';
	selection.select(lineSelector)
		.attr("transform", 'translate(' +horizShift +', ' +vertShift +')')
		.style('display', visibility == 'hidden' ? 'none' : 'initial')
		.style('text-anchor','middle')
		.style('font-size', '12px')
		.style('opacity', 0.7)
		.text(line);
}

export { updateStrips, updateXAxis, updateYAxis, updateYLabel }

import * as d3 from 'd3';
//time-sries
import { } from './StagesTableHelpers';
import { updateStrips, updateColumnHeaders, updateRowHeaders  } from './UpdateStagesTableComponents';
import { updateTitle } from '../UpdateCommonComponents';

/**
* 
* 
*/
function stagesTables(){
	//flags and identifiers
	var svg, chartG
	//setings
	var sizes = {
		isDefault:true,
		//default is MS sizes
		width:500, height:200,
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	var styles ={
		strip:{
		}
	}
	//allds is a useful array of every d
	var data;
	//api-driven update handlers
	var updateSizes, updateBounds, updateTargets, updateDirection
	//element updates
	var mainUpdate

	const dispatcher = d3.dispatch('datapointClick');

	function my(selection){
		//UPDATE FUNCTIONS ----------------------------------------
		updateSizes = function(){
			mainUpdate();
		}
		//main update
		mainUpdate = function(){
			//set strip sizes
			const stripHeight = sizes.chartHeight/data.length;
			const stripMargin = {top:10, bottom:5, left:0, right:0};
			sizes.strip = {
				height:stripHeight,
				width:sizes.width,
				margin:stripMargin,
				chartHeight: stripHeight - stripMargin.top - stripMargin.bottom,
				chartWidth: sizes.chartWidth - stripMargin.left - stripMargin.right
			}
			updateColumnHeaders(svg, data[0].stages, sizes);
			//updateRowHeaders(svg, data, sizes)
			updateStrips(svg, data, sizes, {background:styles.strip.background});
			//updateTitle(svg, sizes, data.info.name);
		}
		
		selection.each(function(chartData){
			//console.log('stages...', chartData)
			if(chartData.stages){
				data = chartData;
			}else if(chartData[0] && !chartData[0].stages){
				//default stages
				data = chartData.map(metric =>({
					...metric,
					stages:[
						{id:'s1', name:'S1', value:40, unit:'%'}, 
						{id:'s2', name:'S2', value:60, unit:'%'}, 
						{id:'s3', name:'S3', value:75, unit:'%'}, 
						{id:'s4', name:'S4', value:90, unit:'%'}
					]
				}))
				//add title again (not good!!!)
				data.info = chartData.info;
			}else{
				data = chartData;
			}
			if(!svg || svg.select('g').node().length === 0){ 
				svg = d3.select(this);
				svg.append('g').attr('class', 'titleG');
				svg.select('g.titleG').append('text').attr('class', 'titleText');
			}

			//dispatcher.on("dataPointClick", function(lineId){
			//});
			mainUpdate();
			
		})
	}
	//api
	//object sizes
	my.sizes = function(value){
		if(!arguments.length)
			return sizes;
		sizes = {...sizes, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSizes === 'function')
			updateSizes();
		return my;
	}
	//styles
	my.styles = function(value){
		if(!arguments.length)
			return styles;
		styles = {...styles, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateStyles === 'function'){
			updateStyles();
		}
		return my;
	}
	//initial return of my to be called on an element with data
	return my;
}

export default stagesTables;
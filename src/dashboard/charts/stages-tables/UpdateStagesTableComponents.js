import * as d3 from 'd3';

const updateStrips = (svg, data=[], sizes, options={}) =>{

	const { width, height, chartWidth, chartHeight, margin, strip } = sizes;
	const { background, hoveredWeekNr } = options;
	const gTranslation = i => 'translate(0 ,' +(margin.top +i*strip.height)+')';
	//helpers
	const backgroundColour = i => {
		if(background.type && background.type.includes('alternate-extended')){
			if(i % 2 === 0){
				return background.colours[0];
			}
			//transpoarent if no 2nd colour
			return background.colours[1] || 'transparent';
		}else{
			return 'transparent';
		}
	}

	//bind
	const updatedStripG = svg.selectAll('g.stripG')
		.data(data, (kpi,i) => i)

	//exit
	updatedStripG.exit().remove();
	//enter
	const stripGEnter = updatedStripG.enter()
		.append('g')
			.attr('class', 'stripG')
			.attr('transform', (kpi,i) => gTranslation(i))
			//merge()
	//background rect
	stripGEnter.append('rect')
		.attr('class', 'backgroundRect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		//need to shift as parent g is not shifted at all - rect starts in top left of strip
		.attr('x', 0)
		.attr('y', 0)
		.attr('fill', (kpi,i) => backgroundColour(i))

	//update
	updatedStripG.attr('transform', (kpi,i) => gTranslation(i))

	updatedStripG.select('rect.backgroundRect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (kpi,i) => backgroundColour(i))

	stripGEnter.each(function(kpi,i){
		const enteredStripG = d3.select(this);
		updateCells(enteredStripG, kpi, sizes)
	})
	//update ds for any updatedStrips
	updatedStripG.each(function(kpi,i){
		const updatedStripG = d3.select(this);
		updateCells(updatedStripG, kpi, sizes)
	})

}

const updateCells = (stripG, kpi, sizes) =>{
	const { strip, margin } = sizes;
	const stages = kpi.stages
	const cellWidth = stages && stages.length != 0 ? strip.chartWidth / stages.length : strip.chartWidth;
	const cellHeight = strip.height;
	//vars
	//for now, unit defaults to % but todo - make in ''
	const metricUnit = kpi.values[0] ? kpi.values[0].unit : ''
	const maxValue = d3.max(kpi.values, v => v.value)
	//helper
	const getStageToUse = (kpi, stage) => {
		//stage can have a pcValue or a value
		//OR the metric can have customised values for some or all stages
		var customStage;
		if(kpi.stages){
			//this particular stage may still be undefined
			customStage = kpi.stages.find(s => s.id === stage.id)
		}
		return customStage || stage;
	}
	const cellValue = (kpi, stage, withUnit=true) =>{
		//stage can have a pcValue or a value
		//OR the metric can have customised values for some or all stages
		const stageToUse = getStageToUse(kpi, stage);
		//todo - change stroke of unit
		return withUnit ? stageToUse.value + stageToUse.unit : stageToUse.value;

	}
	const achieved = (kpi, stage) =>{
		const stageToUse = getStageToUse(kpi, stage);
		//console.log('stageToUse', stageToUse)
		if(stageToUse.unit === '%'){
			if(metricUnit === '%' || 2 == 2){ //temp!!!!!!!!!!!!!!!!!!!!
				return maxValue >= stageToUse.value;
			}else{
				//get as pc of target
				//const pcOfTarget = 
				//return pcOfTarget >= stageToUse.value
			}
			//check if pc of value is above stage.value
		}else{
			//compare raw values
			return maxValue.value >= stageToUse.value;
		}
		//apply fill appropriately
		//temp
	}

	const updatedCellG = stripG.selectAll('g')
		.data(stages, s => s.id)
		//no need for class 
	//ENTER
	const cellGEnter = updatedCellG.enter()
		.append('g')
			.attr('transform', (s,i) => 'translate('+(margin.left+i*cellWidth)+', 0)')
	//add rect for all cells
	cellGEnter.append('rect')
		.attr('class','cellRect bodyCellRect')
		.attr('width', cellWidth)
		.attr('height', cellHeight)
		//.attr('fill', s => achieved(kpi, s) ? '#5a5de6':'none') 
		.attr('fill', s => achieved(kpi, s) ? '#00AFAA':'none')
		.style('stroke-width','0.3')
		.style('stroke-opacity', 0.5)
		.style('stroke','black')

	//update
	updatedCellG.attr('transform', (s,i) => 'translate('+(margin.left+i*cellWidth)+', 0)');

	updatedCellG.selectAll('rect.cellRect')
		.attr('width', cellWidth)
		.attr('height', cellHeight)
		//.attr('fill', s => achieved(kpi, s) ? '#5a5de6':'none') 
		.attr('fill', s => achieved(kpi, s) ? '#00AFAA':'none')

	//cell text
	//enter
	cellGEnter.append('text')
	updateCellText(cellGEnter, cellWidth, cellHeight, s => cellValue(kpi, s))
	//update
	updateCellText(updatedCellG, cellWidth, cellHeight, s => cellValue(kpi, s))

}

const updateCellText = (cellGEnter, cellWidth, cellHeight, textFunc) =>{
	//for now its just enter
	cellGEnter.select('text')
		.attr('transform', 'translate('+(cellWidth*0.5) +',' +(cellHeight*0.5)+')')
		.style('text-anchor', 'middle')
		.style('alignment-baseline', 'middle')
		.style('font-size', '12px')
		.style('opacity', 0.8)
		.text((s,i) => textFunc(s,i))
}

const updateColumnHeaders = (svg, stages, sizes) =>{
	//note - header row goes in top margin
	const { chartWidth, margin } = sizes;
	//vars
	const cellWidth = chartWidth / stages.length;
	const cellHeight = 20;
	//bind
	const updatedHeaderG = svg.selectAll('g.columnHeaderG')
		.data(stages)
	//remove
	updatedHeaderG.exit().remove();
	//enter - g placed in top-left of its rect area
	const headerGEnter = updatedHeaderG.enter()
		.append('g')
			.attr('class', 'headerG columnHeaderG')
			.attr('transform', (s,i) => 
				'translate('+(margin.left+i*cellWidth) +',' +(margin.top-cellHeight) +')')
	//update rects for all col header cells
	headerGEnter.append('rect')
		.attr('class','cellRect headerCellRect')
		.attr('width', cellWidth)
		.attr('height', cellHeight)
		.attr('fill', 'none')
		.style('stroke-width','0.3')
		.style('stroke-opacity', 0.5)
		.style('stroke','black')

	//update
	updatedHeaderG.attr('transform', (s,i) => 
		'translate('+(margin.left+i*cellWidth) +',' +(margin.top-cellHeight) +')')

	updatedHeaderG.select('rect')
		.attr('width', cellWidth)
		.attr('height', cellHeight)

	//text
	//enter
	headerGEnter.append('text')
	updateCellText(headerGEnter, cellWidth, cellHeight, s => s.name)
	//update
	updateCellText(updatedHeaderG, cellWidth, cellHeight, s => s.name)

}

const updateRowHeaders = (svg, metrics, sizes) =>{
	//note - header column goes in left margin
	const { chartHeight, margin } = sizes;
	//vars
	const cellHeight = chartHeight/ metrics.length
	const cellWidth = 80;
	//bind
	const headerG = svg.selectAll('g.rowHeaderG')
		.data(metrics)
	//enter - g placed in top-left of its rect area
	const headerGEnter = headerG.enter()
		.append('g')
			.attr('class', 'headerG rowHeaderG')
			.attr('transform', (d,i) =>
				'translate('+(margin.left-cellWidth) +',' +(margin.top+i*cellHeight) +')')
	//update rect for all col header cells
	headerGEnter.append('rect')
		.attr('class','cellRect headerCellRect')
		.attr('width', cellWidth)
		.attr('height', cellHeight)
		.attr('fill', 'none')
		.style('stroke-width','0.3')
		.style('stroke','black')

	//update text for all row header cells
	updateCellText(headerGEnter, cellWidth, cellHeight, m => m.name)
}

export { updateStrips, updateColumnHeaders, updateRowHeaders }

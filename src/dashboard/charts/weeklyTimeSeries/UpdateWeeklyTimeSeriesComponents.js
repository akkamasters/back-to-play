import * as d3 from 'd3';
import {nest} from 'd3-collection';
import * as fc from 'd3fc';
import { diffInWeeks , addWeeks, addDays, millisecondsToDays } from '../../../data/TimeHelpers';
/**
*
*	
**/

//helpers
//helper
const getDisplayNr = (weekNr, weekNumbers) => {
	//console.log('getDisplayNr weekNr', weekNr)
	const week = weekNumbers.find(wk => wk.weekNr === weekNr)
	if(week){
		return week.displayWeekNr;
	}
	return 1000;
}
//weeklyValues are already filtered for having chronic data
const addBandIndices = (weeklyValues) =>{
	const addBandIndex = (weeksWithIndicesSoFar, week, i) =>{
		if(!week){
			//got to end of weeks
			return weeksWithIndicesSoFar;
		}
		if(i == 0){
			//first week
			return addBandIndex([{...week, bandIndex:1}], weeklyValues[1], i+1);
		}
		const prevIndexedWeek = weeksWithIndicesSoFar[weeksWithIndicesSoFar.length-1];
		const prevIndexedWeekIsLastWeek = prevIndexedWeek.weekNr === week.weekNr - 1;

		const requiredIndex = prevIndexedWeekIsLastWeek ? 
			prevIndexedWeek.bandIndex : prevIndexedWeek.bandIndex +1

		//next call
		const weekToAdd = {...week, bandIndex:requiredIndex}
		const newWeeksWithIndicesSoFar = [...weeksWithIndicesSoFar, weekToAdd];
		return addBandIndex(newWeeksWithIndicesSoFar, weeklyValues[i+1], i+1);
	}
	return addBandIndex([], weeklyValues[0], 0);
}

const updateChronicBands = (stripG, kpi, weekNumbers, scales, sizes, options={}) =>{
	//console.log('KPI....'+kpi.id)
	//console.log('weeklyValues', kpi.weeklyValues)
	const { x, y } = scales;
	const { valueKey, summary, dispatcher } = options;
	//helper
	const getDisplayNr = weekNr => {
		//console.log('getDisplayNr weekNr', weekNr)
		const week = weekNumbers.find(wk => wk.weekNr === weekNr)
		if(week){
			return week.displayWeekNr;
		}
		return 1000;
	}

	//console.log('playerIsCork', playerIsCork)

	//todo - move the data sorting into a separate methid, called from elsewhere earlier up the chain
	//sort data into ds, where each d is a band

	kpi.weeklyValues.forEach(week =>{
		//console.log('chronic for wk '+week.weekNr, week.chronic[valueKey][summary])
	})

	const weeklyValuesWithChronicData = kpi.weeklyValues
		//if there is lower, there wil be upper and mean too
		.filter(week => typeof week.chronic[valueKey][summary].lower === 'number')
		.map(week => ({
			...week,
			lowerY:week.chronic[valueKey][summary].lower,
			upperY:week.chronic[valueKey][summary].upper,
			upper2Y:week.chronic[valueKey][summary].upper2
		}))

	//console.log('weeklyValuesWithChronicData', weeklyValuesWithChronicData);

	const weeklyValuesForBands = addBandIndices(weeklyValuesWithChronicData);
	//temp filter out ones with only 1 value
	const nestedBands = nest().key(wk => wk.bandIndex).entries(weeklyValuesForBands)
		.filter(nest => nest.values.length > 1)
	//console.log('nestedBands', nestedBands)
	//console.log('nestedBands', nestedBands);
	//for each band, if the week previous to the first band week has data,
	//then we add that previous week into teh band, with upper and lower values both
	//equal to y(that weeks value) so it starts from there
	//if not, check the same with 2 weeks before, then 3 weeks before, then 4 weeks (see Ian slack eg)
	//temp for now, just manually add week 27
	//console.log('weeklyValuesForBands', weeklyValuesForBands);
	//console.log('weekNumbers', weekNumbers)
	//console.log('weeklyValues', kpi.weeklyValues)
	//console.log('bands', nestedBands)
	const nestedBandsAmended = nestedBands.map(nest =>{
		//console.log('nest', nest)
		//assume nested weeks are in chrono order
		const firstWeekNrInBand = nest.values[0].weekNr;
		//console.log('firstWeekNrInBand', firstWeekNrInBand)
		const closestPreviousWeekWithData = kpi.weeklyValues.filter(wk => wk.weekNr < firstWeekNrInBand).reverse()[0];
		//console.log('closestPreviousWeekWithData', closestPreviousWeekWithData)
		//add to band if it exists ie if not the start of the chart
		if(closestPreviousWeekWithData){
			//amend the week to add so it has easy access to chronic values
			const amendedWeek = {
				...closestPreviousWeekWithData,
				lowerY:closestPreviousWeekWithData.summaries[valueKey][summary],
				upperY:closestPreviousWeekWithData.summaries[valueKey][summary],
				upper2Y:closestPreviousWeekWithData.summaries[valueKey][summary]
			};
			return {
				...nest,
				values:[amendedWeek, ...nest.values]
			}
		}
		return nest;
		
	})


		//only 1 nest as filtered out other one
		/*
		const playerIsCork = !kpi.values[0] ? false:
		kpi.values[0].id.includes('A1557435989947947')
		if(playerIsCork){
			const week27 = kpi.weeklyValues.find(wk => wk.weekNr === 27)
			const amendedWeek27 ={
				...week27,
				lowerY:week27.summaries[valueKey][summary],
				upperY:week27.summaries[valueKey][summary]
			} 
			return {
				...nest,
				values:[amendedWeek27, ...nest.values]
			};
		}else{
			return nest;
		}
		*/
	//})
	//console.log('nestedBandsAmended', nestedBandsAmended)

	//area (ds here will be the values in a band)
	const area = d3.area()
		.x(d => x(getDisplayNr(d.weekNr)))
		.y1(d => {
			//make a slight gap if both values the same
			//mat need to check and only amend if nextD or prevD also the same
			const shift = d.upperY === d.lowerY ? 2.5 : 0
			return y(d.upperY) - shift;
		})
		.y0(d => {
			const shift = d.upperY === d.lowerY ? 2.5 : 0
			return y(d.lowerY) + shift;
		});

	const area2 = d3.area()
		.x(d => x(getDisplayNr(d.weekNr)))
		.y1(d => {
			return y(d.upper2Y);
		})
		.y0(d => {
			return y(d.upperY);
		});

	//1.BIND
	const updatedBandPath = stripG.selectAll('path.bandPath')
		.data(nestedBandsAmended, d => d.key);

	//2.EXIT
	updatedBandPath.exit().remove();
	//3.ENTER
	//lower path
	const bandPathEnter = updatedBandPath.enter()
		.append('path')
		.attr('class', 'bandPath')
		.style('display', d => {
			return d.values.length < 2 ? 'none' : 'auto'
		})
		.attr('d', d => {
			return area(d.values)
		})
		//.attr('fill', 'aqua')
		.attr('fill','#00dbd4') //25% lighter than teal brand

	//4.UPDATE
	updatedBandPath
		.style('display', d => d.values.length < 2 ? 'none' : 'auto')
		.attr('d', d => {
			return area(d.values)
		})

	//upper band path
	//1.BIND
	const updatedUpperBandPath = stripG.selectAll('path.upperBandPath')
		.data(nestedBandsAmended, d => d.key);
	//2.EXIT
	updatedUpperBandPath.exit().remove();
	//3.ENTER
	const upperBandPathEnter = updatedUpperBandPath.enter()
		.append('path')
		.attr('class', 'upperBandPath')
		.style('display', d => {
			return d.values.length < 2 ? 'none' : 'auto'
		})
		.attr('d', d => {
			return area2(d.values)
		})
		//.attr('fill', '#6495ED')
		.attr('fill','#00AFAA') //teal brand
	//4.UPDATE
	updatedUpperBandPath
		.style('display', d => d.values.length < 2 ? 'none' : 'auto')
		.attr('d', d => {
			return area2(d.values)
		})
}

const updateCtrls = (svg, sizes, summaries, options) =>{
	const { margin, strip } = sizes;
	const { valueKey, summary, dispatcher } = options;
	//5px vert space above, same as title
	const ctrlsG = svg.select('g.ctrlsG')
		.attr('transform', 
			'translate('+(margin.left + strip.margin.left) +',5)');
		//'translate('+(margin.left + strip.margin.left) +','+(margin.top +strip.margin.top -10) +')');

	const buttonHeight = 15;
	const buttonWidth = 30;
	const firstButtonShift = 0;
	const secondButtonShift = buttonWidth +10;
	const thirdButtonShift = 2*(buttonWidth +10);

	summaries.forEach((summaryName,i) =>{
		const horizShift = i * (buttonWidth+10);

		ctrlsG.select('rect.' +summaryName +'BtnRect')
		.attr('transform', 'translate(' +horizShift +',0)')
		.attr('height', buttonHeight)
		.attr('width', buttonWidth)
		.attr('rx', 2)
		.style('stroke', summary == summaryName ? '#B0B0B0' :'#B0B0B0')
		.style('stroke-width', 0.7)
		.attr('fill', summary == summaryName ? '#00AFAA':'none')
		.attr('pointer-events', 'all')
		.style('cursor', 'pointer')
		.html(summaryName)
		.on('click', function(e,d){
			dispatcher.call('weeklyCtrlClick', this, summaryName);
		})

		ctrlsG.select('text.' +summaryName +'BtnText')
			.attr('transform', 'translate(' +(horizShift + buttonWidth/2) +',' +(buttonHeight/2) +')')
			.attr('text-anchor', 'middle')
			.attr('dominant-baseline', 'middle')
			.style('font-size', '10px')
			.style('stroke', summary == summaryName ? 'white' :'#696969')
			.style('cursor', 'pointer')
			//.style('stroke-width', 0.3)
			.text(summaryName)
			.on('click', function(e,d){
				dispatcher.call('weeklyCtrlClick', this, summaryName);
			})
	})
}


//NOTE - TODO - CHANGE FUNCTION NAMES AS ITS ONE LINE, AND THEN HAVE LINE SEGMENTS
//THAT MAKE UP THE LINE
const updateDataLines = (stripG, kpi, weekNumbers, scales, sizes, options={}) =>{
	//todo - put dataLines back using weekNumbers  - if next d is pseudo or is this d is pseudo, then line is dashed
	//lines to and from pseudos are dashed
	const { x, y } = scales;
	const { valueKey, summary, dispatcher } = options;

	//helper
	const getDisplayNr = weekNr => {
		//console.log('getDisplayNr weekNr', weekNr)
		const week = weekNumbers.find(wk => wk.weekNr === weekNr)
		if(week){
			return week.displayWeekNr;
		}
		return 1000;
	}
	//helper
	const getWeek = weekNr => weekNumbers.find(wk => wk.weekNr === weekNr);
	const weekIsPseudo = d => {
		const week = getWeek(d.weekNr);
		if(week && week.isPseudo){
			return true;
		}else{
			return false;
		}
	}
		
	const strokeWidth = (d,i) =>{
		if(dashedLine(d,i)){
			return 1;
		}else{
			return 1.5;
		}
	}
	const strokeColour = (d,i) =>{
		//gets the prev d ie i-1 and calcs gradient
		return 'black';
	}

	/*const showLine = (d,i) =>{
		if(i !== kpi.weeklyValues.length-1){
			const nextD = kpi.weeklyValues[i+1];
			if(nextD.weekNr - d.weekNr <= 12){
				return true;
			}
			return false;
		}
		return false;
	}*/
	//console.log('kpi weeklyValues length', kpi.weeklyValues.length)
	const showLine = (d,i) =>{
		if(i !== kpi.weeklyValues.length-1){
			return true;
		}
		return false;
	}
	const dashedLine = (d,i) =>{
		if(i == kpi.weeklyValues.length-1){
			//display none anyway as its last point
			return false; 
		}
		const nextWeek = weekNumbers.find(wk => wk.weekNr == d.weekNr + 1);
		if(weekIsPseudo(d) || weekIsPseudo(nextWeek)){
				return true;
		}
		return false;
	}
	//1.BIND
	//Note: stripG is positioned at 0,0, so its just a conceptual container for each kpi
	//translations are built into the scales, with a separate yScale for each strip
	const updatedDataLine = stripG.selectAll('line.dataLine')
		.data(kpi.weeklyValues, (d,i) => (d.id ? d.id : i));
	//2.EXIT
	updatedDataLine.exit().remove();
	//3.ENTER
	//console.log('nr of weeklyValues', kpi.weeklyValues.length)
	const dataLineEnter = updatedDataLine.enter()
		.append('line')
			.style('display', (d,i) => showLine(d,i) ? 'initial' : 'none')
			.attr('class', 'dataLine')
			.attr('x1', d => {
				return x(getDisplayNr(d.weekNr))
			})
			.attr('x2', (d,i) => {
				if(!showLine(d,i)){
					return 0;
				}else{
					const nextD = kpi.weeklyValues[i+1];
					return x(getDisplayNr(kpi.weeklyValues[i+1].weekNr));
				}
			})
			.attr('y1', d => {
				//console.log('enter weeknr', d.weekNr)
				//console.log('y1', y(d.summaries[valueKey][summary]))
				return y(d.summaries[valueKey][summary])
			})
			.attr('y2', (d,i) => {
				if(!showLine(d,i)){
					return 0;
				}else{
					const nextD = kpi.weeklyValues[i+1];
					return y(kpi.weeklyValues[i+1].summaries[valueKey][summary])
				}
			})
			.attr("fill", "none")
			.attr("stroke", (d,i) => strokeColour(d,i))
			.attr("stroke-width", (d,i) => strokeWidth(d,i))
			.style('stroke-dasharray', (d,i) => dashedLine(d,i) ? '4' : 'none')

	//4.UPDATE
	updatedDataLine
		.style('display', (d,i) => showLine(d,i) ? 'initial' : 'none')
		.attr('x1', d => {
			return x(getDisplayNr(d.weekNr))
		})
		.attr('x2', (d,i) => {
			if(!showLine(d,i)){
				return 0;
			}else{
				const nextD = kpi.weeklyValues[i+1];
				return x(getDisplayNr(kpi.weeklyValues[i+1].weekNr));
			}
		})
		.attr('y1', d => {
			//console.log('update weeknr', d.weekNr)
			//console.log('y1', y(d.summaries[valueKey][summary]))
			return y(d.summaries[valueKey][summary])
		})
		.attr('y2', (d,i) => {
			if(!showLine(d,i)){
				return 0;
			}else{
				const nextD = kpi.weeklyValues[i+1];
				return y(kpi.weeklyValues[i+1].summaries[valueKey][summary])
			}
		})
		.attr("stroke-width", (d,i) => strokeWidth(d,i))
		.style('stroke-dasharray', (d,i) => dashedLine(d,i) ? '4' : 'none')


}
//renders a new grid line each time so its on top of any other entered elements
const updateGridLine = (stripG, scales, weeklyValues, weekNumbers, options={}) =>{
	const { valueKey, summary, hoveredWeekNr } = options;
	const week = weeklyValues.find(wk => wk.weekNr === hoveredWeekNr);
	//first remove old
	stripG.select('line.gridLine').remove();
	//append new if required
	if(week && !week.isPseudo){
		const displayNr = getDisplayNr(hoveredWeekNr, weekNumbers);

		var x1,x2;
		if(displayNr <= 0.33 * weekNumbers.length){
			//go to left only
			x1 = scales.x(scales.x.domain()[0]);
			x2 = scales.x(displayNr)
		}else if(displayNr <= 0.66 * weekNumbers.length){
			//go both ways
			x1 = scales.x(scales.x.domain()[0]);
			x2 = scales.x(scales.x.domain()[1]);
		}else{
			//go to right
			x1 = scales.x(displayNr);
			x2 = scales.x(scales.x.domain()[1]);
		}

		stripG.append('line')
			.attr('class', 'gridLine')
			.attr('x1', x1)
			.attr('x2', x2)
			.attr('y1', scales.y(week.summaries[valueKey][summary]))
			.attr('y2', scales.y(week.summaries[valueKey][summary]))
			.attr("stroke", 'grey')
			.attr("stroke-width", 1)
			.style('stroke-dasharray','1')
	}
}

//need injury date as there may not be a week 0 if no data
const updateInjuryLine = (svg, sizes, xScale, weekNumbers) =>{

	const injuryWeek = weekNumbers.find(wk => wk.weekNr === 0)
	const horizShift = injuryWeek ? xScale(injuryWeek.displayWeekNr) : 0
	const injuryG = svg.select('g.injuryG')
		.attr('transform', 'translate('+horizShift +',0)')

	//WARNING - MARGINS FOR EACH STRIP ARE ALSO TO BE TAKEN ACCOUNT OF
	injuryG.select('line')
		.style('display', injuryWeek ? 'initial' : 'none')
		.attr('x1', 0)
		.attr('x2', 0)
		.attr('y1', sizes.margin.top)
		.attr('y2', sizes.chartHeight +sizes.margin.top - 10)
		.attr("stroke", 'red')
		.attr("stroke-width", 1)
		.style('stroke-dasharray','1')

	injuryG.select('text')
		.style('display', injuryWeek ? 'initial' : 'none')
		.attr('transform', 'translate(3' +',' +(sizes.margin.top+10) +')')
		.style('text-anchor','start')
		.style('font-size', '12px')
		.style('opacity', 0.6)
		.text('injury');


}

const updateStrips = (svg, data=[], sizes, scales, options={}) =>{
	const { width, height, chartWidth, chartHeight, margin, strip } = sizes;
	const { x, ys } = scales;
	const { valueKey, summary, background, hoveredWeekNr } = options;
	//helpers
	//g starts in top left of strip
	const gTranslation = i => 'translate(0 ,' +(margin.top +i*strip.height)+')';
	//const gTranslation = i => 'translate(0 ,' +(margin.top +i*strip.height)+')';

	const backgroundColour = i => {
		if(background && background.type && background.type.includes('alternate-extended')){
			if(i % 2 === 0){
				return '#DBF1FF';
			}
			return '#F1DBFF';
		}else if(typeof background === 'string'){
			return background;
		}
		return 'transparent';
	}

	//bind
	const updatedStripG = svg.selectAll('g.stripG')
		.data(data, kpi => kpi.id)

	//exit
	updatedStripG.exit().remove();

	//enter
	const stripGEnter = updatedStripG.enter()
		.append('g')
			.attr('class', 'stripG')
			//merge()
			.attr('transform', (kpi,i) => gTranslation(i))
	//ELEMENTS
	//background rects
	stripGEnter.append('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		//need to shift as parent g is not shifted at all - rect starts in top left of strip
		.attr('x', 0)
		.attr('y', 0)//(kpi,i) => i*strip.height + margin.top)
		.attr('fill', (kpi,i) => backgroundColour(i))

	//update
	//update
	updatedStripG.attr('transform', (kpi,i) => gTranslation(i))

	updatedStripG.select('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (kpi,i) => backgroundColour(i))

	//axes
	stripGEnter.append("g").attr("class", "xAxisG");
	stripGEnter.append("g").attr("class", "yAxisG");
	stripGEnter.append("g").attr("class", "rightYAxisG");
	stripGEnter.append("text").attr("class", "yLabelText");
	//datapoints - use each as its a more complex function for each stripG ie several datapoints
	//update ds for any newly entered strips
	stripGEnter.each(function(kpi,i){
		const kpiScales = {
			x:scales.x,
			y:scales.ys[i]
		}
		const stripG = d3.select(this);
		const xAxisOptions = {
			hoveredWeekNr:hoveredWeekNr,
			showNumbers:i === data.length - 1 ? true : false
		}
		const yAxisOptions = {
			hoveredWeekNr:hoveredWeekNr,
			nrTicks:strip.chartHeight < 80 ? 3 : strip.chartHeight < 140 ? 5 : 8
		}
		updateXAxis(stripG, scales.x, sizes, data.weekNumbers, xAxisOptions);
		updateYAxes(stripG, kpiScales.y, sizes, yAxisOptions);
		updateYLabel(stripG, sizes, kpi.shortName);
		//do bands first so underneath - its one path from lower to upper, with fill
		//2 or more points with chronic values makes a band. pseudo poits automatically dont have chronic values
		updateChronicBands(stripG, kpi, data.weekNumbers, kpiScales, sizes.strip, options)

		updateDataLines(stripG, kpi, data.weekNumbers, kpiScales, sizes.strip, options)
		updateGridLine(stripG, kpiScales, kpi.weeklyValues, data.weekNumbers, options)
	})
	//update ds for any updatedStrips
	updatedStripG.each(function(kpi,i){
		const kpiScales = { x:x, y:ys[i] };
		const stripG = d3.select(this);
		const xAxisOptions = {
			hoveredWeekNr:hoveredWeekNr,
			showNumbers:i === data.length - 1 ? true : false
		}
		const yAxisOptions = {
			hoveredWeekNr:hoveredWeekNr,
			nrTicks:strip.chartHeight < 80 ? 3 : strip.chartHeight < 140 ? 5 : 8
		}
		updateXAxis(stripG, scales.x, sizes, data.weekNumbers, xAxisOptions);
		updateYAxes(stripG, kpiScales.y, sizes, yAxisOptions);
		updateYLabel(stripG, sizes, kpi.shortName);
		updateChronicBands(stripG, kpi, data.weekNumbers, kpiScales, sizes.strip, options);
		updateDataLines(stripG, kpi, data.weekNumbers, kpiScales, sizes.strip, options);
		updateGridLine(stripG, kpiScales, kpi.weeklyValues, data.weekNumbers, options)
	})

	//names
	/*if(withNames){
		const textTranslation = ...
		stripGEnter.append('text')
			.attr('transform', textTranslation)
			.attr('text-anchor', 'start')
			.style('font-size', 12)
			.text(m => m.name)
	}*/
}




const updateTitle = (svg, sizes, options) =>{
	const titleG = svg.select('g.titleG')
		.attr('transform', 'translate(0,5)')

	titleG.select('text')
		.style('text-anchor','start')
		//.style('alignment-baseline', 'hanging')
		.style('dominant-baseline', 'hanging')
		.style('font-size', '12px')
		//.style('font-size', '14px') //for pres
		.style('font-weight', 'bold')
		.text('Weekly')

}

const updateXAxis = (selection, scale, sizes, weekNumbers, options={}) =>{
	const { hoveredWeekNr, position, nrTicks } = options;
	const hovered = typeof hoveredWeekNr === 'number';
	console.log('x axis hovered?', hovered)

	if(scale.domain()[0] === 1 && scale.domain()[1] === 1){
		selection.select('g.xAxisG').style('display', 'none')
	}else{
		selection.select('g.xAxisG').style('display', 'initial')

		//helper
		const getWeek = displayWeekNr => weekNumbers.find(wk => wk.displayWeekNr === displayWeekNr);
		const weekIsPseudo = d => {
			const week = getWeek(d);
			if(week && week.isPseudo){
				return true;
			}else{
				return false;
			}
		}
		const axis = fc.axisLabelRotate(fc.axisBottom(scale))
			.ticks(scale.domain()[1] - scale.domain()[0])
			//.tickValues(tickValues)
			//.tickFormat(d3.format("d"))
			//.labelRotate(45)
			.decorate(sel =>{
				//enter
				sel.enter().selectAll("path")
					.style("stroke-width", 0.4)
					.style("stroke", "black")
					//.style("stroke", "white") 
					//.style("stroke", '#f8f9fa')
					.style("opacity",  hovered ? 0.5 : 0.3)

				sel.enter().selectAll("text")
					.style("stroke-width", d => weekIsPseudo(d) ? 1 : 0.6)
					.style("stroke", "black")
					//.style("stroke", "white")
					//.style("stroke", '#f8f9fa')
					//.style("opacity", options.showNumbers ? (weekIsPseudo(d) ? 1 : 0.6) : 0)
					.style("opacity", d => {
						if(!options.showNumbers){
							return 0;
						}else{
							return hovered ? 0.5 : 0.3;

						}
					})
					.style('font-size', d => weekIsPseudo(d) ? 10 : 8)
					//.style('font-size', '12px') //for pres
					.text(d => weekIsPseudo(d) ? '...' : getWeek(d) ? getWeek(d).weekNr : '')
				//update
				sel.selectAll("text")
					.style("stroke-width", d => weekIsPseudo(d) ? 1 : 0.6)
					.style("opacity", d => {
						if(!options.showNumbers){
							return 0;
						}else{
							return hovered ? 0.5 : 0.3;

						}
					})
					.text(d => weekIsPseudo(d) ? '...' : getWeek(d) ? getWeek(d).weekNr : '')

				sel.selectAll("path").style("opacity",  hovered ? 0.5 : 0.3)

				//todo - work out how to transform just the pseudo ...s up by 1.5px
			})

			selection.select('g.xAxisG')
				//.transition(d3.transition().duration(500))
				//.attr("transform", `translate(0, ${sizes.margin.top +sizes.chartHeight})`)
				//note - g has already moved down to topleft of strip
				.attr("transform", `translate(0, ${sizes.strip.margin.top +sizes.strip.chartHeight})`)

			selection.select('g.xAxisG').call(axis)

			selection.select('g.xAxisG').select(".domain")
				.style("stroke-width", 0.4)
				.style("stroke", "black")
				.style("opacity", hovered ? 0.5 : 0.3)
	}
			
}


const updateXLabel = (svg, sizes) =>{
	const { margin, chartHeight } = sizes;
	svg.select('text.xLabel')
		.attr('transform', 'translate(' +10 +',' +(margin.top+chartHeight-2) +')')
		.style('text-anchor','start')
		//.style('alignment-baseline', 'hanging')
		//.style('dominant-baseline', 'hanging')
		.style('font-size', '10px')
		//.style('font-size', '12px') //for pres
		.style('opacity', 0.6)
		.text('Weeks pre/post');
}
/**
*
*
**/
const updateYAxes = (selection, scale, sizes, options={}) =>{
	//left y axis
	updateYAxis(selection, scale, sizes, options);
	//extra right-hand y axis on hover
	updateYAxis(selection, scale, sizes, {...options, position:'right'})
}

const updateYAxis = (selection, scale, sizes, options={}) =>{
	const { chartWidth, margin, strip } = sizes;
	const { hoveredWeekNr, position, nrTicks } = options;
	const hovered = typeof hoveredWeekNr === 'number';

	const selector = position === 'right' ? 'g.rightYAxisG' : 'g.yAxisG';

	const axis = position === 'right' ? d3.axisRight() : d3.axisLeft();
	axis.scale(scale).ticks(nrTicks || 3)

	const translation = position === 'right' ? 
		'translate(' +(margin.left +chartWidth) +',' +0 +')'
		:
		'translate(' +(margin.left +strip.margin.left) +',' +0 +')';

	selection.select(selector)
		.attr("transform", translation)
		.style('display', (position === 'right' && !hovered) ? 'none' : 'initial')
		.call(axis)

	selection.select(selector).selectAll("line")
		.style("stroke-width", 0.6)
		.style("stroke", "grey")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", hovered ? 0.5 : 0.3);

	selection.select(selector).selectAll("path")
		.style("stroke-width", 0.5)
		.style("stroke", "black")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", hovered ? 0.5 : 0.3);

	selection.select(selector).selectAll("text")
		.style("stroke-width", 0.6)
		.style("stroke", "black")
		//.style("stroke", "white")
		//.style("stroke", '#f8f9fa')
		.style("opacity", hovered ? 0.5 : 0.3)
		.style('font-size', 8);
		//.style('font-size', '12px') //for pres
}



const updateYLabel = (selection, sizes, label='', visibility) =>{
	const { margin, chartWidth, chartHeight, strip } = sizes;
	const horizShift = 10; //margin.left + strip.margin.left - 50;
	//stripG is already shifted to topleft of strip
	const vertShift = strip.margin.top + strip.chartHeight *0.6;
	selection.select('text.yLabelText')
		.attr("transform", 'translate(' +horizShift +', ' +vertShift +')')
		.style('text-anchor','start')
		.style('font-size', '10px')
		//.style('font-size', '12px') //for pres
		.style('font-family', 'Arial, Helvetica, sans-serif')
		.style('opacity', 0.7)
		.text(label);
}

const updateVerticalStrips = (svg, weekNumbers, sizes, xScale, options={}) =>{
	const { margin, chartHeight, chartWidth, strip } = sizes;
	const { dispatcher } = options;
	const vertStripHeight = chartHeight - strip.margin.top - strip.margin.bottom;
	const vertStripWidth = chartWidth / weekNumbers.length;
	//y from margin.top+chartHeight   to margin.top  (ie height is chartHeight)

	//const weekWidth = chartWidth / nrOfWeeks (inc pseudos)
	//x width = weekWidth
	//x from margin.left +weekWidth*(displayWeekNr - 0.5)
	//1.BIND
	
	//4.UPDATE
	//updatedVertStripG.select

	const updatedVertStripG = svg.selectAll('g.vertStripG')
		.data(weekNumbers, d => d.weekNr);
	//2.EXIT
	updatedVertStripG.exit().remove();
	//3.ENTER
	const vertStripGEnter = updatedVertStripG.enter()
		.append('g')
		.attr('class', d => 'vertStripG')
		.attr('transform', d => 
			'translate('+(xScale(d.displayWeekNr)-(vertStripWidth/2)) +',' +(margin.top +strip.margin.top) +')')

	//note - append other things before rect so event always fire (or could attach it to g)
	vertStripGEnter.append('rect')
		.attr('x', 0)
		.attr('y', 0)
		.attr('width', vertStripWidth)
		.attr('height', vertStripHeight)
		//.attr('stroke', 'black')
		.attr('pointer-events', 'all')
		//.attr('stroke-width', 1)
		.attr('fill', 'none')
		.style('opacity', 0.3)
		.on('mouseover', function(e, d){
			d3.select(this).attr('fill', '#708090')
			dispatcher.call('verticalStripHover', this, d.weekNr);
		})
		.on('mouseout', function(e, d, i){
			d3.select(this).attr('fill', 'none')
			dispatcher.call('verticalStripHover', this, '');
		})

	//4.UPDATE
	updatedVertStripG.attr('transform', d => 
			'translate('+(xScale(d.displayWeekNr)-(vertStripWidth/2)) +',' +(margin.top +strip.margin.top) +')')

	updatedVertStripG.select('rect')
		.attr('width', vertStripWidth)
		.attr('height', vertStripHeight)
		.on('mouseover', function(e, d){
			d3.select(this).attr('fill', '#708090')
			dispatcher.call('verticalStripHover', this, d.weekNr);
		})
		.on('mouseout', function(e, d, i){
			d3.select(this).attr('fill', 'none')
			dispatcher.call('verticalStripHover', this, '');
		})


		

}

export { updateCtrls, updateInjuryLine, updateStrips, updateTitle, updateVerticalStrips, updateXLabel }

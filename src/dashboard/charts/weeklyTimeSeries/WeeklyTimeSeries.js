import * as d3 from 'd3';
import { flattenDs } from '../../../data/DataHelpers';
import { diffInWeeks , addWeeks, addDays } from '../../../data/TimeHelpers';
//time-sries
import { calcXScale, calcYScale } from './WeeklyTimeSeriesHelpers';
import { updateCtrls, updateInjuryLine,  updateStrips, updateTitle, updateVerticalStrips, updateXLabel } from './UpdateWeeklyTimeSeriesComponents';
import { updateNoDataMesg,} from '../UpdateCommonComponents';

/**
* 
* 
*/
function weeklyTimeSeries(){
	//flags and identifiers
	var svg, chartG;
	//setings
	var sizes = {
		isDefault:true,
		//default is MS sizes
		width:500, height:200,
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	//allds is a useful array of every d
	var data;
	var bounds = {};
	var domains = {};
	var scales = {};
	var background = 'transparent';//{type:'alternate-extended', colours:['aqua', 'grey']}
	var axesToShow=['x','y'];
	var labelsToShow = [];
	var valueKey = 'value'; //can be rawValue, pcValue, movingAvg etc
	var summary = 'total'; //can be max, median, mean, min etc
	var lineSettings = {
		type:'straight',
		text:{
			start:'value',
			end:'value'
		}
	}
	var hoveredWeekNr;
	var withMovingAvg = true;
	//api-driven update handlers
	var updateSizes;
	var mainUpdate, updateScales;
	//dom elements
	//var updateElements
	var onHoverDataLine;

	//todo - put these into one dispatcher
	const dispatcher = d3.dispatch('verticalStripHover');
	const ctrlsDispatcher = d3.dispatch('weeklyCtrlClick');

	function chart(selection){
		updateSizes = function(){
			mainUpdate();
		}
		mainUpdate = function(){
			if(!data[0] || !data[0].weeklyValues || data[0].weeklyValues.length === 0){
				svg.selectAll('g').style('display', 'none');
				updateNoDataMesg(svg, true, 'There is no data for this player in this time period');
			}else{
				updateNoDataMesg(svg, false);
				svg.selectAll('g').style('display', 'initial');
				//set strip sizes
				const stripHeight = sizes.chartHeight/data.length;
				const stripMargin = {top:0, bottom:20, left:0, right:0};
				sizes.strip = {
					height:stripHeight,
					width:sizes.width,
					margin:stripMargin,
					chartHeight: stripHeight - stripMargin.top - stripMargin.bottom,
					chartWidth: sizes.chartWidth - stripMargin.left - stripMargin.right
				}
				updateScales();
				//updateElements();
				const options = {
					axesToShow:axesToShow,
					labelsToShow:labelsToShow,
					background:background,
					valueKey:valueKey,
					summary:summary,
					hoveredWeekNr:hoveredWeekNr
				}
				//for now, just use data[0] to check for existence of values
				updateStrips(svg, data, sizes, scales, options);
				updateInjuryLine(svg, sizes, scales.x, data.weekNumbers);
				updateXLabel(svg, sizes);
				updateTitle(svg, sizes, options);
				//data is summaries
				updateCtrls(svg, sizes, data.summaries, {...options, dispatcher:ctrlsDispatcher})
				//do last so hover/click picked up
				updateVerticalStrips(svg, data.weekNumbers, sizes, scales.x, {dispatcher:dispatcher});
				//hover
				//helper
				//const getWeek = displayWeekNr => data.weekNumbers.find(wk => wk.displayWeekNr === displayWeekNr);
				/*svg.on('mouseover', function(e){
					//console.log('mouseover', e)
					const xValue = Math.round(scales.x.invert(e.x - sizes.margin.left))
					//console.log('xValue', xValue)
					if(hoveredWeekNr !== xValue){
						hoveredWeekNr = xValue;
						console.log('changing to week',getWeek(xValue).weekNr)
						alert('changing to week: '+getWeek(xValue).weekNr)
					}
					//dispatcher.call('dataLineHover', this, ds.id);
				})*/
			}
		}
		updateScales = function(updates){
			const xDomainFull = data.weekNumbers.map(wk => wk.displayWeekNr);
			const xDomain = [d3.min(xDomainFull), d3.max(xDomainFull)]
			scales.x = calcXScale(sizes, {domain:xDomain});
			//console.log('x scale dom', scales.x.domain())
			scales.ys = data.map((kpi,i) => {
				const ds = kpi.weeklyValues.map(v => v.summaries[valueKey][summary]);
				//console.log('ds', ds)
				const lowerBounds = kpi.weeklyValues.map(v => v.chronic[valueKey][summary].lower);
				//console.log('lowerBounds', lowerBounds)
				const upper2Bounds = kpi.weeklyValues.map(v => v.chronic[valueKey][summary].upper2);
				//console.log('upperBounds', upperBounds)
				return calcYScale(sizes.strip, {ds:[...ds, ...lowerBounds, ...upper2Bounds]})
			})

		}	

		onHoverDataLine = function(){
		}
		selection.each(function(chartData){
			console.log('timeSeries data', chartData)
			data = chartData;
			if(!svg || svg.select('g').node().length === 0){
				svg = d3.select(this);
				//title
				svg.append('g').attr('class', 'titleG');
				svg.select('g.titleG').append('text').attr('class', 'titleText');
				//injury
				svg.append('g').attr('class', 'injuryG');
				svg.select('g.injuryG').append('line');
				svg.select('g.injuryG').append('text');
				//x-label (note: y-labels are defined for each stripGEnter)
				svg.append('text').attr('class', 'xLabel');
				//ctrls
				svg.append('g').attr('class', 'ctrlsG')
				if(data.summaries){
					data.summaries.forEach(summary =>{
						svg.select('g.ctrlsG').append('rect').attr('class', summary +'BtnRect');
						svg.select('g.ctrlsG').append('text').attr('class', summary +'BtnText');
					})
				}
			}
			//init updates
			mainUpdate();
		})
	}
	//api
	//object sizes
	chart.sizes = function(value){
		if(!arguments.length)
			return sizes;
		sizes = {...sizes, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSizes === 'function'){
			//note - will need to use this because itneeds to update strip sizes too
			//unless it just calls mainUpdate
			updateSizes();
		}
		return chart;
	}
	//object bounds
	chart.bounds = function(value){
		if(!arguments.length)
			return bounds;
		if(value !== 'no-change'){
			bounds = {...bounds, ...value};
		}
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateBounds === 'function'){
			updateBounds(bounds);
		}
		return chart;
	}
	//object bounds
	chart.domains = function(value){
		if(!arguments.length)
			return domains;
		if(value !== 'no-change'){
			domains = {...domains, ...value};
		}
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateDomains === 'function'){
			updateDomains(domains);
		}
		return chart;
	}
	chart.valueKey = function(value){
		if(!arguments.length)
			return valueKey;
		if(value !== 'no-change'){
			valueKey = value;
		}
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateValueKey === 'function'){
			updateValueKey(valueKey);
		}
		return chart;
	}
	chart.summary = function(value){
		if(!arguments.length)
			return summary;
		if(value !== 'no-change'){
			summary = value;
		}
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSummary === 'function'){
			updateSummary(summary);
		}
		return chart;
	}
	chart.lineSettings = function(value){
		if(!arguments.length)
			return lineSettings;
		if(value !== 'no-change')
			lineSettings = {...lineSettings, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateLineSettings === 'function')
			updateLineSettings(lineSettings);
		return chart;
	}
	//number hoveredWeekNr - set here but still passed down as it saves more logic
	chart.hoveredWeekNr = function(value){
		//console.log('settings bounds', value)
		if(!arguments.length)
			return hoveredWeekNr;
		hoveredWeekNr = value;
		//if passed in before chart is called, then update will not be defined yet 
		if(typeof updateHoveredWeekNr === 'function')
			updateHoveredWeekNr();
		return chart;
	}
	chart.on = function(){
		//how to have several dispatchers? or events at least? need to send ctrl clicks out too 
		//so they can update the table - valuekey and summary need to be stored using useState
		//attach extra arguments 
		let value = dispatcher.on.apply(dispatcher, arguments);
		return value === dispatcher ? chart : value;
	}
	chart.onCtrlClick = function(){
		//how to have several dispatchers? or events at least? need to send ctrl clicks out too 
		//so they can update the table - valuekey and summary need to be stored using useState
		//attach extra arguments 
		let value = ctrlsDispatcher.on.apply(ctrlsDispatcher, arguments);
		return value === ctrlsDispatcher ? chart : value;
	}

	//initial return of chart to be called on an element with data
	return chart;
}

export default weeklyTimeSeries;
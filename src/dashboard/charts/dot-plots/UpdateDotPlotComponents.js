import * as d3 from 'd3';
import AntiCollisionShifter from './AntiCollisionShifter'

const updateLegend = (svg, sizes, nrOfStrips=5, direction='horizontal') =>{
	//for now, just keep r teh same regardless of screen size, so it fits margin at bottom
	const radius = 5//strip.height/10; //8;
	const items = ['Highest', 'Latest', 'Previous'];
	const y = sizes.margin.top +sizes.chartHeight + 22;
	items.forEach((text,i) =>{
		const r = text === 'Highest' 
		const x = 10 + i *80; 
		svg.select('circle.legendCircle-'+(i+1))
			.attr('cx', x)
			.attr('cy', y)
			.attr("r", text === 'Highest' ? radius*1.25 : radius) //8;)
			.attr('stroke-width', text === 'Most recent' ? 1.5 : 0)
			.attr('stroke', 'black')
			.attr('filter', text === 'Previous' ? 'url(#blur)' : 'none')
			//.style("fill", text === 'Highest' ? '#5a5de6' : '#6495ED')
			//highest = teal, rest = //25% lighter than teal
			.style("fill", text === 'Highest' ? '#00AFAA' : '#00dbd4') 
			.style("fill-opacity", text === 'Highest' ? 1 : 0.5)
			.style("stroke-opacity", (text === 'Highest' || text === 'Most recent') ? 1 : 0.5);

		svg.select('text.legendText-'+(i+1))
			.attr('transform', 'translate('+(x +15) +',' +y +')')
			.style('text-anchor', 'start')
			.style('alignment-baseline', 'middle')
			.style('font-size','10px')
			//.style('font-size','12px') //for pres
			.text(text)

	})

}
//todo  use .merge()
const updateDatapoints = (stripG, kpi, maxD, stripWidth, stripHeight, yScale, options) =>{
	const { direction, hoveredWeekNr } = options;	
	const radius = d3.min([stripHeight/14, 10]); //8;
	//helper
	//legacy
	const translation = d =>{
		if(direction === 'horizontal'){
			return 'translate('+d.chartY +',' +d.chartX +')';
		}
		return 'translate('+stripWidth*0.5 +',' +yScale(d.value) +')';
	}

	const translationNew = d =>{
		if(direction === 'horizontal'){
			return 'translate('+d.chartY +',' +d.chartX +')';
		}
		return 'translate('+d.chartX +',' +d.chartY +')';
	}
	//add maxD to value if it is not there due to time cutoff
	//and order fro shifts, so mostRecent and max values must be at start
	//values are most recent first
	const maxDMustBeAdded = maxD && !kpi.values.find(d => d.date === maxD.date);
	var valuesForShifts;
	if(maxDMustBeAdded){
		//console.log('maxd must be added', maxD)
		return [maxD, ...kpi.values]
	}else{
		//move existing max to front, if it exists (ie not empty array)
		if(kpi.values.length !== 0){
			const max = d3.max(kpi.values, d => yScale(d.value));
			//earliest date max was reached
			const allMaxDs = kpi.values.filter(d => yScale(d.value) === max);
			const earliestMaxD = allMaxDs[allMaxDs.length-1];
			const otherValues = kpi.values.filter(d => d.date !== earliestMaxD.date);
			valuesForShifts = [earliestMaxD, ...otherValues];
		}else{
			valuesForShifts = kpi.values; //ie []
		}
	}
	//console.log('valuesForShifts', valuesForShifts)

	const antiCollisionShifter = new AntiCollisionShifter();
	//todo - impl chartX, chartY for vertical direction (from translation func above)
	const unshiftedData = valuesForShifts.map(d =>({ 
			...d,
			chartX:stripHeight*0.5, //todo - vert
			chartY:yScale(d.value), //todo - vert
	}));
	//console.log('unshiftedData', unshiftedData)

	const shiftedDs = antiCollisionShifter.createShifts(unshiftedData, {y:yScale, r:radius})
	//console.log('shiftedDs', shiftedDs)
	//move max and mostRecent from front to back so rendered last
	//so ds are in chrono order now
	const orderedValues = [...shiftedDs].reverse()
	//console.log('orderedValues', orderedValues)

	//move max to back so rendred
	//bind

	//use scale to take account of poss numberorder options
	//const max = d3.max(ds, d => yScale(d.value))
	//console.log('maxes', ds.map(d => yScale(d.value) === max))
	//earliest date max was reached
	//const firstMaxD = ds.find(d => yScale(d.value) === max);
	//most recent may not be at start if max was added
	const mostRecentD = orderedValues.find(d => d.date === d3.max(orderedValues, d => d.date))
	//console.log('mostRecentD', mostRecentD)
	//todo - get rid of these as repeated from above
	const max = d3.max(kpi.values, d => yScale(d.value));
	const allMaxDs = kpi.values.filter(d => yScale(d.value) === max);
	const earliestMaxD = allMaxDs[allMaxDs.length-1];

	//helpers for render attrs
	const isFirstMax = d => earliestMaxD.date === d.date;
	const isMostRecent = d => mostRecentD.date === d.date;

	const circleRadius = d =>{
		if(isFirstMax(d)){
			return 1.5 * radius;
		}
		if(isMostRecent(d)){
			return 1.33 * radius;
		}
		return radius;
	}
	/*var endValues;
	//if one exists they both will, but may be the same point
	if(firstMaxD){
		if(firstMaxD.date !== mostRecentD.date){
			endValues = [firstMaxD, mostRecentD];
		}else{
			endValues = [firstMaxD];
		}
	}else{
		endValues = [];
	}*/
	//const otherValues = ds.filter(d => !isFirstMax(d) && !isMostRecent(d))
	//most recent is rendered last, as it may be hidden behind first max otherwise
	//first max is rendered second, so that no other ds affect its visibility
	//const orderedValues = [...otherValues, ...endValues];
	//console.log('orderedValues', orderedValues)
	//move most recent and highest to the end as they render on top
	const updatedDatapointG = stripG.selectAll("g.datapointG")
		.data(orderedValues, v => v.date)
		.attr('transform', d => translation(d));
	//exit
	updatedDatapointG.exit().attr('class', 'exit').remove();
	//update
	updatedDatapointG.classed('enter', false);
	//assume horizontal for now (i == 0 for most recent value)
	updatedDatapointG.select('circle')
		.attr('stroke-width', d => isMostRecent(d) ? 3 : 0)
		.attr('stroke', 'black')
		.attr("r", d => circleRadius(d))
		.attr('filter', d => (isFirstMax(d) || isMostRecent(d)) ? 'none' : 'url(#blur)')
		//.style("fill", d => isFirstMax(d) ? '#0000FF' : '#6495ED')
		.style("fill", d => 
				hoveredWeekNr === d.weekNr ? '#ffa343' : isFirstMax(d) ? '#00AFAA' : '#00dbd4')
		.style("fill-opacity", d => (isFirstMax(d) || hoveredWeekNr === d.weekNr) ? 1 : 0.5)
		.style("stroke-opacity", d => 
			(isFirstMax(d) || isMostRecent(d) || hoveredWeekNr === d.weekNr) ? 1 : 0.5);
	//enter
	const datapointGEnter = updatedDatapointG.enter()
		.append("g")
		  	.attr("class", d => "enter datapointG ")
		  	.attr('transform', d => translation(d));
		  	//todo - use merge instead of repeating code here and in update above
	datapointGEnter
		.append("circle")
			.attr('stroke-width', d => isMostRecent(d) ? 3 : 0)
			.attr('stroke', 'black')
			.attr("r", d => circleRadius(d))
			.attr('filter', d => (isFirstMax(d) || isMostRecent(d)) ? 'none' : 'url(#blur)')
			//.style("fill", d => isFirstMax(d) ? '#0000FF' : '#6495ED')
			.style("fill", d => 
				hoveredWeekNr === d.weekNr ? '#708090' : isFirstMax(d) ? '#00AFAA' : '#00dbd4')
			.style("fill-opacity", d => (isFirstMax(d) /*|| isMostRecent(d)*/) ? 1 : 0.5)
			.style("stroke-opacity", d => (isFirstMax(d) || isMostRecent(d)) ? 1 : 0.5)
		    .style('cursor', 'pointer');


}

const updateStrips = (svg, data=[], maxDs, sizes, yScale, options={}) =>{
	//console.log('maxDs', maxDs)
	const { width, height, chartWidth, chartHeight, margin, strip } = sizes;
	const { direction, background, withNames, hoveredWeekNr } = options;
	//helpers
	const gTranslation = i =>{
		if(direction === 'horizontal'){
			return 'translate(0 ,' +(margin.top +i*strip.height)+')';
		}
		return 'translate('+(margin.left +i*strip.width) +', 0)';
	}
	const textTranslation = direction === 'horizontal' ?
			'translate(' +20 +','+strip.height*0.5+')' :
		    'translate('+strip.width*0.5 +',' +(margin.top +chartHeight +margin.bottom*0.3)+')';

	const backgroundColour = i => {
		if(background.type && background.type.includes('alternate-extended')){
			if(i % 2 === 0){
				return background.colours[0];
			}
			//transpoarent if no 2nd colour
			return background.colours[1] || 'transparent';
		}else{
			return 'transparent';
		}
	}

	//bind
	const updatedStripG = svg.selectAll('g.stripG')
		//.data(data, metric => metric.id) previous
		.data(data, (kpi,i) => i) //new
		//.attr('transform', (m,i) => gTranslation(i))
		//note - previous workd here too but not fro sparklines - should learn why - because
		//its probably better to bind to kpi.id so it doesnt change but maybe deosnt matter
	
	//exit
	updatedStripG.exit().remove();
	//enter
	const stripGEnter = updatedStripG.enter()
		.append('g')
			.attr('class', 'stripG')
			//merge()
			.attr('transform', (m,i) => gTranslation(i))
	//ELEMENTS
	//background rects
	stripGEnter.append('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (m,i) => backgroundColour(i))

	//UPDATE
	updatedStripG.attr('transform', (m,i) => gTranslation(i))
	updatedStripG.select('rect')
		.attr('width', strip.width)
		.attr('height', strip.height)
		.attr('fill', (m,i) => backgroundColour(i))
	//datapoints - use each as its a more complex function for each stripG ie several datapoints
	//update ds for any newly entered strips
	stripGEnter.each(function(kpi,i){
		const maxD = maxDs.find(d => {
			const splitId = d.id.split('-');
			const kpiIdForD = splitId[splitId.length-1]; 
			return kpiIdForD === kpi.id;
		})
		updateDatapoints(d3.select(this), kpi, maxD, strip.width, strip.height, yScale, options);
	})
	//update ds for any updatedStrips
	updatedStripG.each(function(kpi,i){
		const maxD = maxDs.find(d => {
			const splitId = d.id.split('-');
			const kpiIdForD = splitId[splitId.length-1];
			return kpiIdForD === kpi.id;
		})
		updateDatapoints(d3.select(this), kpi, maxD, strip.width, strip.height, yScale, options);
	})
}

//todo - use .join, or at least the update exit pattern
const updateTargetLines = (svg, sizes, yScale, targets, direction) =>{
	const { chartWidth, chartHeight, margin } = sizes;
	//helper
	//shift the g to the correct pos on the y scale
	const gTranslation = t =>{
		if(direction === 'horizontal'){
			return 'translate('+yScale(t.value) +',0)';
		}
		return 'translate(0,' +yScale(t.value) +')'
	}
	//bind
	const updatedTargetG = svg.selectAll('g.targetG')
		.data(targets, t => t.id)
		.attr('transform', t => gTranslation(t));

	const targetGEnter = updatedTargetG.enter()
		.append('g')
		.attr('class', 'targetG')
		.attr('transform', t => gTranslation(t));
		//merge()?
	//g has already been shifted to correct pos on y scale
	targetGEnter.append('line')
		.attr('x1', t => direction === 'horizontal' ? 0 : margin.left)
	    .attr('y1', t => direction === 'horizontal' ? margin.top : 0)
	    .attr('x2', t => direction === 'horizontal' ? 0 : margin.left +chartWidth)
	    .attr('y2', t => direction === 'horizontal' ? margin.top +chartHeight : 0)
	    .style('stroke', 'black')
	    .style('opacity', 0.3)
	    .style("stroke-width", 0.7)
	    .style('stroke-dasharray', '4');

	targetGEnter.append('text')
		.attr('transform', d => direction === 'horizontal' ? 
			'translate(0, ' +(margin.top +chartHeight +12) +')' : 
			'translate('+(margin.left +chartWidth +20) +', 0)'
		)
		.style('text-anchor', direction === 'horizontal' ? 'middle' : 'start')
		.style('font-size', '12px')
		.text(t => t.name)

	updatedTargetG.select('line')
		.attr('x1', t => direction === 'horizontal' ? 0 : margin.left)
	    .attr('y1', t => direction === 'horizontal' ? margin.top : 0)
	    .attr('x2', t => direction === 'horizontal' ? 0 : margin.left +chartWidth)
	    .attr('y2', t => direction === 'horizontal' ? margin.top +chartHeight : 0)

	updatedTargetG.select('text')
		.attr('transform', d => direction === 'horizontal' ? 
			'translate(0, ' +(margin.top +chartHeight +12) +')' : 
			'translate('+(margin.left +chartWidth +20) +', 0)'
		)
		.style('text-anchor', direction === 'horizontal' ? 'middle' : 'start')
		.text(t => t.name)



}

const updateYAxis = (svg, scale, sizes, direction) =>{
	const selection = svg.select('g.yAxisG');
	var axis, translation;
	if(direction === 'horizontal'){
		axis = d3.axisTop().scale(scale).ticks(5);
		//slightly offset so not covered by background which is updated
		translation = 'translate(0,' +sizes.margin.top*0.9 +')';
	}else{
		axis = d3.axisLeft().scale(scale).ticks(5);
		//slightly offset so not covered by background which is updated
		translation = 'translate(' +sizes.margin.left*0.9 +',0)';
	}

	selection.attr("transform", translation).call(axis)

	selection.selectAll("line")
		.style("stroke-width", 0.8)
		.style("stroke", "black")
		.style("opacity", 0.5);

	selection.selectAll("path")
		.style("stroke-width", 0.8)
		.style("stroke", "black")
		.style("opacity", 0.5);

	selection.selectAll("text")
		.style('font-size', '10px')
		.style("stroke-width", 0.3)
		.style("stroke", "black")
		.style('fill', 'black')
		.style("opacity", 0.5)
		.text(d => d + '%')
}

export { updateLegend, updateDatapoints, updateStrips, updateTargetLines, updateYAxis }
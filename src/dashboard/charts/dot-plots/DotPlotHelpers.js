import * as d3 from 'd3';

export const calcYScale = (sizes, options={}) => {
	const { chartHeight, chartWidth, margin } = sizes
	var requiredDomain;
	if(options.domain){
		requiredDomain = options.domain;
	}else if(options.ds){
		const lowerY = d3.min(options.ds);
		const upperY = d3.max(options.ds);
		//note - this doesnt handle negative numbers properly. Set min y to 0 for now
		/*const dataRange = upperY -lowerY;
		const _lowerYMinus = lowerY - 0.15*dataRange;
		const lowerYMinus = _lowerYMinus > 0 ? _lowerYMinus : 0
		const upperYPlus = upperY + 0.15*dataRange;


		requiredDomain = options.numberOrder === 'high-to-low' ? 
			[upperYPlus, lowerYMinus] : [lowerYMinus, upperYPlus];
			*/
		requiredDomain = options.numberOrder === 'high-to-low' ? 
			[upperY, lowerY] : [lowerY, upperY];
	}else{
		requiredDomain = [0,100];
	}
	var requiredRange;
	if(options.direction == 'horizontal'){
		requiredRange = [margin.left, chartWidth +margin.left];
	}else{
		requiredRange = [chartHeight + margin.top, margin.top];
	}

	return d3.scaleLinear()
			.domain(requiredDomain)
			.nice()
			.range(requiredRange)
}
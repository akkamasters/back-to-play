import * as d3 from 'd3';
//time-sries
import { calcYScale } from './DotPlotHelpers';
import { updateLegend, updateStrips, updateTargetLines, updateYAxis } from './UpdateDotPlotComponents';
import { updateTitle } from '../UpdateCommonComponents';
/**
* 
* 
*/
function dotPlots(){
	//flags and identifiers
	var svg, chartG
	//setings
	var sizes = {
		isDefault:true,
		//default is MS sizes
		width:500, height:200,
		margin: {top:300 *0.11, bottom:300 *0.18, left:600 *0.2, right:600*0.1},
		chartWidth:600 *0.7, chartHeight:300 *0.68
	}
	//todo - impl this as api
	var styles ={
		strip:{
		}
	}
	//allds is a useful array of every d
	var data;
	var bounds, yScale = '';
	var direction='horizontal';
	var pointsToShow = {
		weeklyMax:undefined,//2,
		nrWeeks:undefined,
		max:undefined//8 //so 4 weeks
	}
	var hoveredWeekNr = '';
	var targets = [];
	//api-driven update handlers
	var updateSizes, updateBounds, updateTargets, updateDirection
	//element updates
	var mainUpdate

	const dispatcher = d3.dispatch('datapointClick');

	function my(selection){
		//UPDATE FUNCTIONS ----------------------------------------
		updateSizes = function(){
			//for now just main
			mainUpdate()
		}
		//main update
		mainUpdate = function(){
			const stripHeight = sizes.chartHeight/data.length;
			const stripMargin = {top:10, bottom:5, left:0, right:0};
			sizes.strip = {
				height:stripHeight,
				width:sizes.width,
				margin:stripMargin,
				chartHeight: stripHeight - stripMargin.top - stripMargin.bottom,
				chartWidth: sizes.chartWidth - stripMargin.left - stripMargin.right
			}
			//we will display most recent 3 values for each
			//note - may be better to keep all values too

			//for each kpi, we need to 
			// - get max and add it to most recent
			//calc bounds from that
			const maxDs = data.map(kpi =>
				kpi.values.find(d => d.value === d3.max(kpi.values, v => v.value))
			).filter(d => d)

			const recentData = data.map(kpi =>{
				var valuesToShow = kpi.values;
				if(pointsToShow.weeklyMax){
					//filter ds for each week so only top two
					valuesToShow = valuesToShow.filter((d,i) =>{
						//todo - change .value to using valueKey object
						const topTwo = valuesToShow.filter(d2 => d2.weekNr === d.weekNr)
												   .sort((d1,d2) => d2.value - d1.value)
												   .slice(0,2)

						return topTwo.find(d2 => d2.date === d.date);
					})
				}
				const sortedSlicedValues = pointsToShow.max ? 
					valuesToShow.sort((v1,v2) => v2.date - v1.date).slice(0, pointsToShow.max) :
					valuesToShow.sort((v1,v2) => v2.date - v1.date);

				return{
					...kpi,
					values:sortedSlicedValues
				}
			})

			const actualDs = recentData.map(kpi => kpi.values)
									   .reduce((v1,v2) => [...v1, ...v2], [])
			//console.log('actualDs', actualDs)
			const targetDs = data.map(kpi => kpi.targets)
								 .reduce((t1,t2) => [...t1, ...t2], [])

			//maxds may or may not already be in array - repeats wont affect scales
			const allDs = [...actualDs, ...targetDs, ...maxDs];
			//console.log('allDs', allDs)
			//if no ds, then we go from 0 to 100
			const domain = actualDs.length === 0 ? [0, 100] : undefined;
			//console.log('manual domain: ', domain)
			yScale = calcYScale(sizes, 
				{ds:allDs.map(d => d.value), direction:direction, domain:domain});
			//console.log('yScale domain', yScale.domain())

			updateYAxis(svg, yScale, sizes, direction);
			const stripOptions = {
				direction:direction,
				background:styles.strip.background,
				hoveredWeekNr:hoveredWeekNr
			}
			//pass maxds separately in case they have been cut out due to time elapsing
			updateStrips(svg, recentData, maxDs, sizes, yScale, stripOptions)
			updateTargetLines(svg, sizes, yScale, targets, direction);
			//updateTitle(svg, sizes, data.info.name)
			updateLegend(svg, sizes, data.length, direction);
		}
		
		selection.each(function(chartData){
			data = chartData;
			//console.log('dotPlots data', data);
			if(!svg || svg.select('g').node().length === 0){
				svg = d3.select(this);
				svg.append("g").attr("class", "yAxisG");
				svg.append('line').attr('class', 'baseLine');
				svg.append('g').attr('class', 'titleG');
				svg.select('g.titleG').append('text').attr('class', 'titleText');
				//legend
				svg.append('circle').attr('class', 'legendCircle-1')
				svg.append('circle').attr('class', 'legendCircle-2')
				svg.append('circle').attr('class', 'legendCircle-3')
				svg.append('text').attr('class', 'legendText-1')
				svg.append('text').attr('class', 'legendText-2')
				svg.append('text').attr('class', 'legendText-3')
			}

			//dispatcher.on("dataPointClick", function(lineId){
			//});
			mainUpdate();
			
		})
	}
	//api
	//object sizes
	my.sizes = function(value){
		if(!arguments.length)
			return sizes;
		//console.log('updating sizes...', value.height)
		//console.log('updating sizes...width', value.width)
		sizes = {...sizes, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateSizes === 'function'){
			updateSizes();
		}
		return my;
	}
	//styles
	my.styles = function(value){
		if(!arguments.length)
			return styles;
		styles = {...styles, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateStyles === 'function'){
			updateStyles();
		}
		return my;
	}
	//object scales
	my.bounds = function(value){
		//console.log('settings bounds', value)
		if(!arguments.length)
			return bounds;
		bounds = {...bounds, ...value};
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateBounds === 'function')
			updateBounds();
		return my;
	}
	//object scales
	my.targets = function(value){
		//console.log('settings bounds', value)
		if(!arguments.length)
			return targets;
		targets = value;
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateTargets === 'function')
			updateTargets();
		return my;
	}
	//object scales
	my.direction = function(value){
		//console.log('settings bounds', value)
		if(!arguments.length)
			return direction;
		direction = value;
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateDirection === 'function')
			updateDirection();
		return my;
	}
	//number hoveredWeekNr
	my.hoveredWeekNr = function(value){
		//console.log('settings bounds', value)
		if(!arguments.length)
			return hoveredWeekNr;
		hoveredWeekNr = value;
		//if passed in before chart is called, then updateSizes will not be defined yet 
		if(typeof updateHoveredWeekNr === 'function')
			updateHoveredWeekNr();
		return my;
	}
	//initial return of my to be called on an element with data
	return my;
}

export default dotPlots;
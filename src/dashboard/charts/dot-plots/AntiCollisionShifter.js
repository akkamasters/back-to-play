import { sameDay } from '../../../data/TimeHelpers'
//todo - think about class syntax instead
function AntiCollisionShifter(){
}

/**
*  defauklt shift is 5, ie assumes radius of circle / half rect width is 5
**/

//todo - impl option to start with existing chart values in the cache
AntiCollisionShifter.prototype.createShifts = function(datapoints, scales, initCache=[]){
	const { x, y, r } = scales
	const radius = r || 10;
	const range = radius*0.5;
	//helpers
	//note - need to add scales to this check too, when we will check if its
	//todo - check within a certain range, rather than exact equality, also days or weeks etc

	const checkSame = (p1, p2, range) => {
		//console.log('checkSame p2', p2)
		//console.log('check?', p1.chartX == p2.chartX && p1.chartY == p2.chartY)
		if(!range || range === 0){
			return p1.chartX == p2.chartX && p1.chartY == p2.chartY
		}else{
			return Math.abs(p1.chartX - p2.chartX) < range && Math.abs(p1.chartY - p2.chartY) < range;
		}
	}

	const checkPoints = (d, remainingPoints, cache) =>{
		//move points that are within half a radius of each other
		const range = radius*0.5;
		const shiftUnit = radius;
		const match = cache.find(datapoint => checkSame(d, datapoint, range))
		//find the nearest point with no match (right shifts allowed only atm)
		if(match){
			//console.log('match!', d)
			//console.log('cache', cache)
			//todo - if the one it clashes with is highest, then shoft should be greater as circle is bigger
			//const newShift = { x:8, y:8 }; //r = 8 (10 for highest)
			//create new and increase shift value
			var newShift;
			if(!d.shift){
				//d has not been shifted yet. start bytrying left=1
				newShift = {left:1}
			}else if(d.shift.left){
				//d has been shifted left by a certain amount and hasnt worked so shift right by same amount
				newShift = {right:d.shift.left}
			}else{
				//d.shift.right, so d has been shifted right, so now shift left by an extra unit
				newShift = {left:d.shift.right+1}
			}
			const origChartX = d.origChartX || d.chartX; //will be d.chartX the first time
			const shiftAmount = newShift.left ? -(newShift.left * shiftUnit) : newShift.right * shiftUnit;
			//console.log('shiftAmount', shiftAmount)
			const shiftedPoint = {
				...d,
				origChartX:origChartX, //will be d.chartX the first time
				chartX:origChartX +shiftAmount,//d.chartX-radius,
				shift:newShift
				//chartY:d.chartY// -16, 
				/*shift:{ 
					x:d.shift.x +newShift.x,
					y:d.shift.y +newShift.y
				}*/
			}
			//console.log('newShift', newShift)
			//console.log('shiftedPoint', shiftedPoint)
			//check this shiftedPoint again in case in needs to shift more
			return checkPoints(shiftedPoint, remainingPoints, cache)
		}else{
			//point is fine - check if it was the last point
			if(remainingPoints.length > 0)
				return checkPoints(remainingPoints[0], remainingPoints.slice(1), [...cache, d])
			else
				return [...cache, d]
		}
	}

	if(!datapoints[0])
		return []
	else{
		/*const chartDatapoints = datapoints.map(d =>({
			...d, chartX:x(d.xValue), chartY:y(d.yValue)
		}))*/
		//apply shifts
		return checkPoints(datapoints[0], datapoints.slice(1), initCache)
	}
} 

export default AntiCollisionShifter

/*
AntiCollisionShifter.prototype.createShifts = function(datapoints, otherPlayersDatapoints){
	//note - inreality, need to test for same week if scale is going up in weeks?
	//or maybe same three days? depends on scale
	//assume one player per datapoint set for now
	const playerId = datapoints[0].players[0]
	//collision avoidance

	const datapointsWithShifts = datapoints.map(datapoint =>{
		//console.log('datapoint', datapoint)
		const nrPlayersWithSameValue = otherPlayersDatapoints
			.map(playerDs => playerDs.find(d => sameDay(d.xValue, datapoint.xValue)))
			.filter(d => d && d.yValue === datapoint.yValue)
			.length
		//move circle by 1 diameter up for each identical value
		const shiftDown = 5 * nrPlayersWithSameValue
		return {...datapoint, shiftDown: shiftDown}
	})
	return datapointsWithShifts
} 

export default AntiCollisionShifter

*/

# Back To Play #

### What is this repository for? ###

This repository is for demonstration of code purposes only. This project is at an early iteration stage. 
It is incomplete, although it is live and already being used by paying customers and not available publicly. Please contact the developer should you wish to view a demonstration.
See screenshots below for a sampe of the dashboard.

### About the project ###

Back To Play is a front-end subproject within a larger sports data web application which serves elite sports clients who subscribe (premiership football clubs). 
The purpose is to create a dashboard which helps clients to track the progress that injured footballers are making in their recovery. 
Raw data is obtained via laser technology which has been installed in stadiums and then processed into a database which this project accesses via fetch calls to a server.
The data used in this prototype was incomplete, and hence there are some large gaps in the data which the system handles by removing those periods from the x-axis in the weekly section.

There follows some screenshots of the application. 

Initial View on load (for a selected player with pseudo name)
![image](images/layout_no_hover.png)


User hovers week 29 (ie 29 weeks post injury)
![image](images/hover_week_29.png)

User hovers week 30 

![image](images/hover_week_30.png)

User hovers week 31 

![image](images/hover_week_31.png)


### Methodology and Progress ###

This is an early stage prototype that was created from scratch over the past 6 weeks in collaboration with two senior sports scientists at professional football clubs. 
This involved a series of 5 meetings, with iterative agile development in between each meeting following feedback. 

During this 6 weeks, the app was already used successfully to track an injured player’s progress, and has provided insights which this club have credited with improving the rehabilitation process of the player. 
It is now undergoing a second wider trial with more than one injured player at the same club. Feedback is also being elicited from other clubs, all of which has been extremely positive so far.

This prototype was implemented by one developer (Peter Domokos) from scratch within 6 weeks at speed. 
Each weekly iteration required significant changes, and hence the current code is not fully tested, and neither has it been refactored sufficiently. 
It is envisaged that it will undergo substantial changes following the current trial, and hence there has been a trade-off in terms of the code quality in places, for the time-being. 
There is also a small degree of hardcoding of variables which will be written to and read from the database in future.

### Implementation ###

This project is built with ReactJS and D3.js, and sits as a subproject within a non-React application. The server and database are shared with the larger project.

As the wider software team are as yet uncertain whether or not to embrace React fully, the developer took the decision that the D3 code would not be integrated with React in ways that they might have done in other circumstances (eg D3 for data processing, React for the rendering). This would have made switching out of React more difficult for the team, should they wish to. Instead, the ‘black-box’ approach has been taken, with a useEffect hook used to render the code for each chart which makes u the entire dashboard. React handles the player selection and some other user interaction from the charts, via D3’s dispatch pattern, which propagates chart interactions out of the d3 code. This setup effectively means that each d3 chart could be dropped into a non-React html page.

D3’s reusable chart pattern has been used for all charts, with an api for chart settings. The enter-update-exit pattern is used throughout, often at two levels of depth, though no use has been made if the merge() function at this stage, as it was desired for options to be left as open as possible.

Styling is achieved with Material-UI for a CSS-in-JS solution, in addition to d3 chaining of styles for svg elements.
